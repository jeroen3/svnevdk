/******************************************************************************
 * Project    : Embedded Vision Design
 * Copyright  : 2012 HAN Embedded Systems Engineering
 * Author     : Hugo Arends (Project owner, 2010-2011)
 *              Oscar Starink & Sander Hoevers (EVDK hardware, 2011-2012)
 *              Sjoerd Driessen (Histogram displaying, 2011-2012)
 *
 * Description: Implementation file for the main program
 *
 ******************************************************************************
  Change History:

    Version 2.1 - November 2012
    > Enabled source image LUT
    > Added save options for source and destination images
    > Fixed memory leak when displaying images

    Version 2.0 - October 2012
    > Merged EVDK hardware + histogram

    Version 1.0 - September 2011
    > Initial revision

******************************************************************************/
#include <QDebug>
#include <QDir>
#include <QFileDialog>
#include "mainwindow.h"
#include "operators.h"
#include "VisionSet.h"
#include "ui_mainwindow.h"

// ----------------------------------------------------------------------------
// Global variables
// ----------------------------------------------------------------------------
QString  info;
uint16_t lut;
uint16_t imgCount;

// ----------------------------------------------------------------------------
// Function implementations
// ----------------------------------------------------------------------------
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    int16_t x,y;

    ui->setupUi(this);

    HideHistogram();

    ui->labelSrcImageFromEVDK->setVisible(0);

    imgCount = 0;

    // Reset images
    imgSrc.height  = IMG_HEIGHT;
    imgDst.height  = IMG_HEIGHT;
    imgSrc.width   = IMG_WIDTH;
    imgDst.width   = IMG_WIDTH;
    histSrc.height = HIST_HEIGHT;
    histDst.height = HIST_HEIGHT;
    histSrc.width  = HIST_WIDTH;
    histDst.width  = HIST_WIDTH;
    imgDstDisplayed.width  = IMG_WIDTH;
    imgDstDisplayed.height = IMG_HEIGHT;

    for(y=0; y<(imgSrc.height); y++)
    {
        for(x=0; x<(imgSrc.width); x++)
        {
            imgSrc.data[y][x] = 255;
        }
    }

    for(y=0; y<(imgDst.height); y++)
    {
        for(x=0; x<(imgDst.width); x++)
        {
            imgDst.data[y][x] = 255;
        }
    }

    ui->LUTSrc->addItem("Stretch"); // 0
    ui->LUTSrc->addItem("Clip");    // 1
    ui->LUTSrc->addItem("Binary");  // 2
    ui->LUTSrc->addItem("Labeled"); // 3

    ui->LUTDst->addItem("Stretch"); // 0
    ui->LUTDst->addItem("Clip");    // 1
    ui->LUTDst->addItem("Binary");  // 2
    ui->LUTDst->addItem("Labeled"); // 3

    QStringList list;
    list.append("Qt Vision set 1"); // 0
    list.append("Qt Vision set 2"); // 1
    list.append("Qt Vision set 3"); // 2
    list.append("Qt Vision set 4"); // 3
    list.append("Qt Vision set 5"); // 4
    list.append("Qt Vision set 6"); // 5
    list.append("Qt Vision set 7"); // 6
    list.append("Qt Vision set 8"); // 7
    list.append("Qt Vision set 9"); // 8

    ui->Operator->addItems(list);
    ui->LUTSrc->setCurrentIndex(LUT_CLIP);
    DrawImage(ui->gvSrc, &sceneSrcImg, ui->LUTSrc, &imgSrc);
    DrawHistogram(&imgSrc, &histSrc);
    DrawHistogramInfoSrc();
    DrawHist(ui->gvHisSrc, &sceneSrcHist, &histSrc);

    memcpy(&imgDstDisplayed, &imgDst, sizeof(image_t));
    DrawImage(ui->gvDst, &sceneDstImg, ui->LUTDst, &imgDstDisplayed);
    ui->LUTDst->setCurrentIndex(LUT_STRETCH);
    DrawHistogram(&imgDst, &histDst);
    DrawHistogramInfoDst();
    DrawHist(ui->gvHisDst, &sceneDstHist, &histDst);

    devKit = NULL;
    ContinuousRunning = false;
    ui->actionClose->setDisabled(true);
    ui->actionOpen->setEnabled(true);

    ui->pushButtonSingle->setVisible(0);
    ui->pbOpenImage->setMaximumWidth(181);

    setMode(RUNMODE_DEFAULT);

    // Display the histograms
    ui->actionShow_histogram_src->setChecked(true);
    ui->actionShow_histogram_dst->setChecked(true);
    on_actionShow_histogram_src_triggered(true);
    on_actionShow_histogram_dst_triggered(true);

    ui->statusBar->showMessage(QString("(c) 2012 HAN ESE minor EVD1: Develop vision operators in ANSI-C"), 0);
}

MainWindow::~MainWindow()
{
    if(devKit != NULL)
    {
        devKit->close();
        delete devKit;
    }
    delete ui;
}

void MainWindow::exitClickedHandler()
{
    if(devKit != NULL)
    {
        devKit->close();
    }

    qApp->exit(0);
}

void MainWindow::openImageFile()
{
    switch(runmode)
    {
        // --------------------------------------------------------------------
        case RUNMODE_DEFAULT:
        {
            QString filename = QFileDialog::getOpenFileName(this,
                               tr("Open Document"),
                               QDir::currentPath(),
                               tr("Image files (*.bmp *.gif *.jpg *.jpeg *.png);;"));

            if(!filename.isNull())
            {
               int16_t x,y;

               // Read and scale image
               QImage qimage = QImage(filename, 0);
               QImage convert = qimage.convertToFormat(QImage::Format_Indexed8, Qt::AutoColor);
               QImage scaled = convert.scaled(imgSrc.width, imgSrc.height, Qt::IgnoreAspectRatio, Qt::FastTransformation);

               // Convert image
               for(y=0; y<(imgSrc.height); y++)
               {
                   for(x=0; x<(imgSrc.width); x++)
                   {
                      imgSrc.data[y][x] = scaled.pixel(x, y);
                   }
               }

               // Display image
               DrawImage(ui->gvSrc, &sceneSrcImg, ui->LUTSrc, &imgSrc);
               DrawHistogram(&imgSrc, &histSrc);
               DrawHistogramInfoSrc();
               DrawHist(ui->gvHisSrc, &sceneSrcHist, &histSrc);

               // Recalculate destination image
               executeVisionSet();

               //info.clear();
               //info.append(QString("File succesfully opened and converted to int8 "));
               //info.append(filename);
               //ui->plainTextInfo->appendPlainText(info);
               //info.clear();
            }
            else
            {
               info.clear();
               info.append(QString("Could not open file "));
               info.append(filename);
               ui->plainTextInfo->appendPlainText(info);
            }
        }break;

        // --------------------------------------------------------------------
        case RUNMODE_SNAPSHOT:
        {
            devKit->verbinding.SetImageNr(1);
            devKit->verbinding.SetProgramma(0);
            devKit->verbinding.SetMode(Mode_Single);
            devKit->clearInfo();
        }
        break;

        // --------------------------------------------------------------------
        case RUNMODE_CONTINUOUS:
        {
            if(ContinuousRunning == true)
            {
                devKit->verbinding.SetMode(Mode_None);
                ui->pbOpenImage->setText(QString("Continuous"));
                ContinuousRunning = false;
            }
            else
            {
                devKit->verbinding.SetImageNr(ui->spinBoxDevKit->value());
                devKit->verbinding.SetProgramma(ui->Operator->currentIndex()+1);
                devKit->verbinding.SetMode(Mode_Continuous);

                ui->pbOpenImage->setText(QString("Stop"));
                ui->plainTextInfo->clear();
                ContinuousRunning = true;
            }
        }break;

        // --------------------------------------------------------------------
        case RUNMODE_FILE_TO_DEVBOARD:
        {

            QString filename = QFileDialog::getOpenFileName(this,
                                                           tr("Open Document"),
                                                           QDir::currentPath(),
                                                           tr("Image files (*.bmp *.gif *.jpg *.jpeg *.png);;"));

            if(!filename.isNull())
            {
               int16_t x,y;

               // Read and scale image
               imgSrc.width = IMG_WIDTH;
               imgSrc.height = IMG_HEIGHT;

               QImage qimage = QImage(filename, 0);
               QImage convert = qimage.convertToFormat(QImage::Format_Indexed8, Qt::AutoColor);
               QImage scaled = convert.scaled(imgSrc.width, imgSrc.height, Qt::IgnoreAspectRatio, Qt::FastTransformation);

               // Convert image
               for(y=0; y<(imgSrc.height); y++)
               {
                   for(x=0; x<(imgSrc.width); x++)
                   {
                      imgSrc.data[y][x] = scaled.pixel(x, y);
                   }
               }

               // Display image
               DrawImage(ui->gvSrc, &sceneSrcImg, ui->LUTSrc, &imgSrc);
               DrawHistogram(&imgSrc, &histSrc);
               DrawHistogramInfoSrc();
               DrawHist(ui->gvHisSrc, &sceneSrcHist, &histSrc);

               ui->plainTextInfo->clear();
               devKit->verbinding.SetImageNr(ui->spinBoxDevKit->value());
               devKit->verbinding.SetProgramma(ui->Operator->currentIndex()+1);
               devKit->verbinding.InjectImage(imgSrc);
               devKit->verbinding.SetMode(Mode_Inject);
               devKit->clearInfo();
            }
        }break;

        default:
        {
            ; // Do nothing
        }break;
    }
}

void MainWindow::aboutClicked()
{
    info.clear();
    ui->plainTextInfo->clear();
    info.append(QString("(c) 2011 HAN ESE minor EVD1: Develop vision operators in ANSI-C\n\n"));
    info.append(QString("Template project provided by H. Arends"));
    ui->plainTextInfo->appendPlainText(info);
}

void MainWindow::executeVisionSet()
{
    imgCount=0;

    switch(ui->Operator->currentIndex())
    {
    case 0:
    {
        VisionSet1(this, &imgSrc, &imgDst);

    }break;
    case 1:
    {
        VisionSet2(this, &imgSrc, &imgDst);

    }break;
    case 2:
    {
        VisionSet3(this, &imgSrc, &imgDst);

    }break;
    case 3:
    {
        VisionSet4(this, &imgSrc, &imgDst);

    }break;
    case 4:
    {
        VisionSet5(this, &imgSrc, &imgDst);

    }break;
    case 5:
    {
        VisionSet6(this, &imgSrc, &imgDst);

    }break;
    case 6:
    {
        VisionSet7(this, &imgSrc, &imgDst);

    }break;
    case 7:
    {
        VisionSet8(this, &imgSrc, &imgDst);

    }break;
    case 8:
    {
        VisionSet9(this, &imgSrc, &imgDst);

    }break;
    default:
    {

    }break;
    }

    // Display operator info
    ui->plainTextInfo->clear();
    ui->plainTextInfo->appendPlainText(info);
    info = QString("");
}

void MainWindow::LUTSrcChanged()
{
    DrawImage(ui->gvSrc, &sceneSrcImg, ui->LUTSrc, &imgSrc);
    DrawHistogram(&imgSrc, &histSrc);
    DrawHistogramInfoSrc();
    DrawHist(ui->gvHisSrc, &sceneSrcHist, &histSrc);
}

void MainWindow::LUTDstChanged()
{
    DrawImage(ui->gvDst, &sceneDstImg, ui->LUTDst, &imgDstDisplayed);
    //DrawHistogram(&imgDst, &histDst);
    //DrawHistogramInfoDst();
    //DrawHist(ui->gvHisDst, &sceneDstHist, &histDst);
}

void MainWindow::DrawImage(QGraphicsView *gview, QGraphicsScene *scene, QComboBox *lut, image_t *img)
{
    int16_t x,y;
    uint8_t *rgb = new uint8_t[3 * img->height * img->width];

    // Translate to RGB depending on the LUT
    switch(lut->currentIndex())
    {
    case 1: // Clip
    {
        for(y=0; y<(img->height); y++)
        {
            for(x=0; x<(img->width); x++)
            {
                rgb[(((y*img->width)+x)*3)+0] = img->data[y][x];
                rgb[(((y*img->width)+x)*3)+1] = img->data[y][x];
                rgb[(((y*img->width)+x)*3)+2] = img->data[y][x];
            }
        }
    }break;
    case 2: // Binary
    {
        for(y=0; y<(img->height); y++)
        {
            for(x=0; x<(img->width); x++)
            {
                if(img->data[y][x] == 0)
                {
                    rgb[(((y*img->width)+x)*3)+0] = 255;
                    rgb[(((y*img->width)+x)*3)+1] = 255;
                    rgb[(((y*img->width)+x)*3)+2] = 255;
                }
                else
                {
                    rgb[(((y*img->width)+x)*3)+0] = 0;
                    rgb[(((y*img->width)+x)*3)+1] = 0;
                    rgb[(((y*img->width)+x)*3)+2] = 0;
                }
            }
        }
    }break;
    case 3: // Labeled
    {
        for(y=0; y<(img->height); y++)
        {
            for(x=0; x<(img->width); x++)
            {
                if(img->data[y][x] == 0)
                {
                    // Background
                    rgb[(((y*img->width)+x)*3)+0] = 255;
                    rgb[(((y*img->width)+x)*3)+1] = 255;
                    rgb[(((y*img->width)+x)*3)+2] = 255;
                }
                else if ((img->data[y][x] % 3) == 2)
                {
                    rgb[(((y*img->width)+x)*3)+0] = 0;
                    rgb[(((y*img->width)+x)*3)+1] = 255;
                    rgb[(((y*img->width)+x)*3)+2] = 0;
                }
                else if((img->data[y][x] % 3) == 1)
                {
                    rgb[(((y*img->width)+x)*3)+0] = 255;
                    rgb[(((y*img->width)+x)*3)+1] = 0;
                    rgb[(((y*img->width)+x)*3)+2] = 0;
                }
                else
                {
                    rgb[(((y*img->width)+x)*3)+0] = 0;
                    rgb[(((y*img->width)+x)*3)+1] = 0;
                    rgb[(((y*img->width)+x)*3)+2] = 255;
                }
            }
        }
    }break;
    default: // Stretch
    {
        image_t tmp;

        tmp.height = IMG_HEIGHT;
        tmp.width  = IMG_WIDTH;

        vContrastStretch(img, &tmp, 0, 255);

        for(y=0; y<(tmp.height); y++)
        {
            for(x=0; x<(tmp.width); x++)
            {
                rgb[(((y*tmp.width)+x)*3)+0] = tmp.data[y][x];
                rgb[(((y*tmp.width)+x)*3)+1] = tmp.data[y][x];
                rgb[(((y*tmp.width)+x)*3)+2] = tmp.data[y][x];
            }
        }
    }break;
    }

    // Draw the image
    QImage qimage(rgb, img->width, img->height, QImage::Format_RGB888);
    QPixmap qpixmap = QPixmap::fromImage(qimage);
    scene->clear();
    scene->addPixmap(qpixmap);
    gview->setScene(scene);

    // Clean up
    delete rgb;
}

void MainWindow::DrawHist(QGraphicsView *gview, QGraphicsScene *scene, draw_hist_t *hist)
{
    int16_t x,y;
    uint8_t *rgb = new uint8_t[3 * hist->height * hist->width];

    // Translate to RGB
    for(y=0; y<(hist->height); y++)
    {
        for(x=0; x<(hist->width); x++)
        {
            if(hist->data[y][x] == 0)
            {
                // Background
                rgb[(((y*hist->width)+x)*3)+0] = 255;
                rgb[(((y*hist->width)+x)*3)+1] = 255;
                rgb[(((y*hist->width)+x)*3)+2] = 255;
            }
            else if ((hist->data[y][x] % 3) == 2)
            {
                rgb[(((y*hist->width)+x)*3)+0] = 0;
                rgb[(((y*hist->width)+x)*3)+1] = 255;
                rgb[(((y*hist->width)+x)*3)+2] = 0;
            }
            else if((hist->data[y][x] % 3) == 1)
            {
                rgb[(((y*hist->width)+x)*3)+0] = 255;
                rgb[(((y*hist->width)+x)*3)+1] = 0;
                rgb[(((y*hist->width)+x)*3)+2] = 0;
            }
            else
            {
                rgb[(((y*hist->width)+x)*3)+0] = 0;
                rgb[(((y*hist->width)+x)*3)+1] = 0;
                rgb[(((y*hist->width)+x)*3)+2] = 255;
            }
        }
    }

    // Draw the image
    QImage qimage(rgb, hist->width, hist->height, QImage::Format_RGB888);
    QPixmap qpixmap = QPixmap::fromImage(qimage);
    scene->clear();
    scene->addPixmap(qpixmap);
    gview->setScene(scene);

    /*
    QGraphicsScene *scene = new QGraphicsScene;
    scene->addPixmap(qpixmap);
    gview->setScene(scene);
    */

    // Clean up
    delete rgb;
}

void MainWindow::saveSrcBmpClicked()
{
    QString filename = QFileDialog::getSaveFileName(this,
                                                    tr("Save Document"),
                                                    QDir::currentPath() + "/src.bmp",
                                                    tr("Images (*.bmp);;"));

    if(!filename.isNull())
    {
        QPixmap qpixmap = QPixmap::grabWidget(this->ui->gvSrc);
        QPixmap copy = qpixmap.copy(1,1,176,144);
        copy.save(filename, "BMP");
    }
    else
    {
        info.clear();
        info.append(QString("Could not open file "));
        info.append(filename);
        ui->plainTextInfo->appendPlainText(info);
    }
}

void MainWindow::saveDstBmpClicked()
{
    QString filename = QFileDialog::getSaveFileName(this,
                                                    tr("Save Document"),
                                                    QDir::currentPath() + "/dst.bmp",
                                                    tr("Images (*.bmp);;"));

    if(!filename.isNull())
    {
        QPixmap qpixmap = QPixmap::grabWidget(this->ui->gvDst);
        QPixmap copy = qpixmap.copy(1,1,176,144);
        copy.save(filename, "BMP");
    }
    else
    {
        info.clear();
        info.append(QString("Could not open file "));
        info.append(filename);
        ui->plainTextInfo->appendPlainText(info);
    }
}

void MainWindow::saveSrcJlClicked()
{
    QString filename = QFileDialog::getSaveFileName(this,
                                                    tr("Save Document"),
                                                    QDir::currentPath() + "/src.jl",
                                                    tr("Document files (*.jl);;"));

    if(!filename.isNull())
    {
        saveJL(&imgSrc, filename);
    }
    else
    {
        info.clear();
        info.append(QString("Could not open file "));
        info.append(filename);
        ui->plainTextInfo->appendPlainText(info);
    }
}

void MainWindow::saveDstJlClicked()
{
    QString filename = QFileDialog::getSaveFileName(this,
                                                    tr("Save Document"),
                                                    QDir::currentPath() + "/dst.jl",
                                                    tr("Document files (*.jl);;"));

    if(!filename.isNull())
    {
        saveJL(&imgDstDisplayed, filename);
    }
    else
    {
        info.clear();
        info.append(QString("Could not open file "));
        info.append(filename);
        ui->plainTextInfo->appendPlainText(info);
    }
}

void MainWindow::saveJL(image_t *img, QString filename)
{
    char p[10];

    QFile file(filename);
    if(!file.open(QIODevice::WriteOnly))
    {
        info.clear();
        info.append(QString("Could not open file "));
        info.append(filename);
        ui->plainTextInfo->appendPlainText(info);

        return;
    }

    file.write("JL ByteImage ");

    itoa(img->height, p, 10);
    file.write(p);
    file.write(" ");

    itoa(img->width, p, 10);
    file.write(p);
    file.write(" ");

    p[0]=0x0A;
    file.write(p, 1);

    file.write((char *)(&img->data[0][0]), img->height*img->width);

    p[0]=0x0A;
    file.write(p, 1);

    file.write("JL");

    file.close();

    info.clear();
    info.append(QString("File successfully exported "));
    info.append(filename);
    ui->plainTextInfo->appendPlainText(info);
}

void MainWindow::on_actionShow_histogram_src_triggered(bool checked) // laat histogram van de src image zien.
{
    if(checked == true)
    {
        setGeometry(QRect((x()+ 8), (y() +30), width() + EXPAND, height()));
        ui->gvDst->setGeometry(QRect(ui->gvDst->x() + EXPAND, ui->gvDst->y(),ui->gvDst->width() , ui->gvDst->height()));
        ui->labelDstImage->setGeometry(QRect(ui->labelDstImage->x() + EXPAND, ui->labelDstImage->y(), ui->labelDstImage->width(), ui->labelDstImage->height()));
        ui->Operator->setGeometry(QRect(ui->Operator->x() + EXPAND, ui->Operator->y(), ui->Operator->width(), ui->Operator->height()));
        ui->spinBoxDevKit->setGeometry(QRect(ui->spinBoxDevKit->x() + EXPAND, ui->spinBoxDevKit->y(), ui->spinBoxDevKit->width(), ui->spinBoxDevKit->height()));
        ui->labelImgNr->setGeometry(QRect(ui->labelImgNr->x() + EXPAND, ui->labelImgNr->y(), ui->labelImgNr->width(), ui->labelImgNr->height()));
        ui->LUTDst->setGeometry(QRect(ui->LUTDst->x() + EXPAND, ui->LUTDst->y(), ui->LUTDst->width(), ui->LUTDst->height()));
        ui->plainTextInfo->setGeometry(QRect(ui->plainTextInfo->x() + EXPAND, ui->plainTextInfo->y(), ui->plainTextInfo->width(), ui->plainTextInfo->height()));
        ui->labelInfo->setGeometry(QRect(ui->labelInfo->x() + EXPAND, ui->labelInfo->y(), ui->labelInfo->width(), ui->labelInfo->height()));
        ui->pbExit->setGeometry(QRect(ui->pbExit->x() + EXPAND,ui->pbExit->y(), ui->pbExit->width(), ui->pbExit->height()));
        ui->gvHisSrc->show();
        ui->labelSrcHist->show();
        ui->gvHisDst->setGeometry(QRect(ui->gvHisDst->x() + EXPAND, ui->gvHisDst->y(), ui->gvHisDst->width(), ui->gvHisDst->height()));
        ui->labelDstHist->setGeometry(QRect(ui->labelDstHist->x() + EXPAND, ui->labelDstHist->y(), ui->labelDstHist->width(), ui->labelDstHist->height()));

        ui->labelLaagSrc->show();
        ui->labelMiddelSrc->show();
        ui->labelHoogSrc->show();

        ui->labelLaagDst->setGeometry(QRect(ui->labelLaagDst->x() + EXPAND, ui->labelLaagDst->y(), ui->labelLaagDst->width(), ui->labelLaagDst->height()));
        ui->labelMiddelDst->setGeometry(QRect(ui->labelMiddelDst->x() + EXPAND, ui->labelMiddelDst->y(), ui->labelMiddelDst->width(), ui->labelMiddelDst->height()));
        ui->labelHoogDst->setGeometry(QRect(ui->labelHoogDst->x() + EXPAND, ui->labelHoogDst->y(), ui->labelHoogDst->width(), ui->labelHoogDst->height()));

        ui->labelMeanSrc->show();
        ui->labelMinSrc->show();
        ui->labelMaxSrc->show();

        ui->labelMeanDst->setGeometry(QRect(ui->labelMeanDst->x() + EXPAND, ui->labelMeanDst->y(), ui->labelMeanDst->width(), ui->labelMeanDst->height()));
        ui->labelMinDst->setGeometry(QRect(ui->labelMinDst->x() + EXPAND, ui->labelMinDst->y(), ui->labelMinDst->width(), ui->labelMinDst->height()));
        ui->labelMaxDst->setGeometry(QRect(ui->labelMaxDst->x() + EXPAND, ui->labelMaxDst->y(), ui->labelMaxDst->width(), ui->labelMaxDst->height()));
    }
    else
    {
        setGeometry(QRect((x()+ 8), (y() +30), width() - EXPAND, height()));
        ui->gvDst->setGeometry(QRect(ui->gvDst->x() - EXPAND, ui->gvDst->y(),ui->gvDst->width() , ui->gvDst->height()));
        ui->labelDstImage->setGeometry(QRect(ui->labelDstImage->x() - EXPAND, ui->labelDstImage->y(), ui->labelDstImage->width(), ui->labelDstImage->height()));
        ui->Operator->setGeometry(QRect(ui->Operator->x() - EXPAND, ui->Operator->y(), ui->Operator->width(), ui->Operator->height()));
        ui->spinBoxDevKit->setGeometry(QRect(ui->spinBoxDevKit->x() - EXPAND, ui->spinBoxDevKit->y(), ui->spinBoxDevKit->width(), ui->spinBoxDevKit->height()));
        ui->labelImgNr->setGeometry(QRect(ui->labelImgNr->x() - EXPAND, ui->labelImgNr->y(), ui->labelImgNr->width(), ui->labelImgNr->height()));
        ui->LUTDst->setGeometry(QRect(ui->LUTDst->x() - EXPAND, ui->LUTDst->y(), ui->LUTDst->width(), ui->LUTDst->height()));
        ui->plainTextInfo->setGeometry(QRect(ui->plainTextInfo->x() - EXPAND, ui->plainTextInfo->y(), ui->plainTextInfo->width(), ui->plainTextInfo->height()));
        ui->labelInfo->setGeometry(QRect(ui->labelInfo->x() - EXPAND, ui->labelInfo->y(), ui->labelInfo->width(), ui->labelInfo->height()));
        ui->pbExit->setGeometry(QRect(ui->pbExit->x() - EXPAND,ui->pbExit->y(), ui->pbExit->width(), ui->pbExit->height()));
        ui->gvHisSrc->hide();
        ui->labelSrcHist->hide();
        ui->gvHisDst->setGeometry(QRect(ui->gvHisDst->x() - EXPAND, ui->gvHisDst->y(), ui->gvHisDst->width(), ui->gvHisDst->height()));
        ui->labelDstHist->setGeometry(QRect(ui->labelDstHist->x() - EXPAND, ui->labelDstHist->y(), ui->labelDstHist->width(), ui->labelDstHist->height()));

        ui->labelLaagSrc->hide();
        ui->labelMiddelSrc->hide();
        ui->labelHoogSrc->hide();

        ui->labelLaagDst->setGeometry(QRect(ui->labelLaagDst->x() - EXPAND, ui->labelLaagDst->y(), ui->labelLaagDst->width(), ui->labelLaagDst->height()));
        ui->labelMiddelDst->setGeometry(QRect(ui->labelMiddelDst->x() - EXPAND, ui->labelMiddelDst->y(), ui->labelMiddelDst->width(), ui->labelMiddelDst->height()));
        ui->labelHoogDst->setGeometry(QRect(ui->labelHoogDst->x() - EXPAND, ui->labelHoogDst->y(), ui->labelHoogDst->width(), ui->labelHoogDst->height()));

        ui->labelMeanSrc->hide();
        ui->labelMinSrc->hide();
        ui->labelMaxSrc->hide();

        ui->labelMeanDst->setGeometry(QRect(ui->labelMeanDst->x() - EXPAND, ui->labelMeanDst->y(), ui->labelMeanDst->width(), ui->labelMeanDst->height()));
        ui->labelMinDst->setGeometry(QRect(ui->labelMinDst->x() - EXPAND, ui->labelMinDst->y(), ui->labelMinDst->width(), ui->labelMinDst->height()));
        ui->labelMaxDst->setGeometry(QRect(ui->labelMaxDst->x() - EXPAND, ui->labelMaxDst->y(), ui->labelMaxDst->width(), ui->labelMaxDst->height()));
    }
}


void MainWindow::on_actionShow_histogram_dst_triggered(bool checked) // laat histogram van de dst image zien.
{
    if(checked == true)
    {
        setGeometry(QRect((x()+ 8), (y() +30), width() + EXPAND, height()));
        ui->plainTextInfo->setGeometry(QRect(ui->plainTextInfo->x() + EXPAND, ui->plainTextInfo->y(), ui->plainTextInfo->width(), ui->plainTextInfo->height()));
        ui->labelInfo->setGeometry(QRect(ui->labelInfo->x() + EXPAND, ui->labelInfo->y(), ui->labelInfo->width(), ui->labelInfo->height()));
        ui->pbExit->setGeometry(QRect(ui->pbExit->x() + EXPAND,ui->pbExit->y(), ui->pbExit->width(), ui->pbExit->height()));
        ui->gvHisDst->show();
        ui->labelDstHist->show();

        ui->labelLaagDst->show();
        ui->labelMiddelDst->show();
        ui->labelHoogDst->show();

        ui->labelMeanDst->show();
        ui->labelMinDst->show();
        ui->labelMaxDst->show();
    }
    else
    {
        setGeometry(QRect((x()+ 8), (y() +30), width() - EXPAND, height()));
        ui->plainTextInfo->setGeometry(QRect(ui->plainTextInfo->x() - EXPAND, ui->plainTextInfo->y(), ui->plainTextInfo->width(), ui->plainTextInfo->height()));
        ui->labelInfo->setGeometry(QRect(ui->labelInfo->x() - EXPAND, ui->labelInfo->y(), ui->labelInfo->width(), ui->labelInfo->height()));
        ui->pbExit->setGeometry(QRect(ui->pbExit->x() - EXPAND,ui->pbExit->y(), ui->pbExit->width(), ui->pbExit->height()));
        ui->gvHisDst->hide();
        ui->labelDstHist->hide();

        ui->labelLaagDst->hide();
        ui->labelMiddelDst->hide();
        ui->labelHoogDst->hide();

        ui->labelMeanDst->hide();
        ui->labelMinDst->hide();
        ui->labelMaxDst->hide();
    }    
}

void MainWindow::HideHistogram() // laat alles wat bij de histogramman hoort verdwijnen. en zet alles al op de juiste plaats.
{
    ui->gvHisDst->hide();       ui->gvHisSrc->hide();
    ui->labelDstHist->hide();   ui->labelSrcHist->hide();
    ui->labelLaagSrc->hide();   ui->labelLaagDst->hide();
    ui->labelMiddelSrc->hide(); ui->labelMiddelDst->hide();
    ui->labelHoogSrc->hide();   ui->labelHoogDst->hide();
    ui->labelMeanSrc->hide();   ui->labelMeanDst->hide();
    ui->labelMinSrc->hide();    ui->labelMinDst->hide();
    ui->labelMaxSrc->hide();    ui->labelMaxDst->hide();

    ui->gvHisSrc->setGeometry(QRect(230, 30, ui->gvHisSrc->width(), ui->gvHisSrc->height()));
    ui->labelSrcHist->setGeometry(QRect(230, 10, ui->labelSrcHist->width(), ui->labelSrcHist->height()));
    ui->gvHisDst->setGeometry(QRect(420, 30, ui->gvHisDst->width(), ui->gvHisDst->height()));
    ui->labelDstHist->setGeometry(QRect(420, 10, ui->labelDstHist->width(), ui->labelDstHist->height()));
    ui->labelLaagSrc->setGeometry(QRect(183, 223, ui->labelLaagSrc->width(), ui->labelLaagSrc->height()));
    ui->labelLaagDst->setGeometry(QRect(373, 223, ui->labelLaagDst->width(), ui->labelLaagDst->height()));
    ui->labelMiddelSrc->setGeometry(QRect(183, 123, ui->labelMiddelSrc->width(), ui->labelMiddelSrc->height()));
    ui->labelMiddelDst->setGeometry(QRect(373, 123, ui->labelMiddelDst->width(), ui->labelMiddelDst->height()));
    ui->labelHoogSrc->setGeometry(QRect(183, 23, ui->labelHoogSrc->width(), ui->labelHoogSrc->height()));
    ui->labelHoogDst->setGeometry(QRect(373, 23, ui->labelHoogDst->width(), ui->labelHoogDst->height()));
    ui->labelMeanSrc->setGeometry(QRect(230, 230, ui->labelMeanSrc->width(), ui->labelMeanSrc->height()));
    ui->labelMeanDst->setGeometry(QRect(420, 230, ui->labelMeanDst->width(), ui->labelMeanDst->height()));
    ui->labelMinSrc->setGeometry(QRect(330, 230, ui->labelMinSrc->width(), ui->labelMinSrc->height()));
    ui->labelMinDst->setGeometry(QRect(520, 230, ui->labelMinDst->width(), ui->labelMinDst->height()));
    ui->labelMaxSrc->setGeometry(QRect(430, 230, ui->labelMaxSrc->width(), ui->labelMaxSrc->height()));
    ui->labelMaxDst->setGeometry(QRect(620, 230, ui->labelMaxDst->width(), ui->labelMaxDst->height()));
}

void MainWindow::DrawHistogramInfoSrc() // laat juiste informatie zien in de labels die bij de histogram van de src hoort.
{
    QString highSrc;
    QString meanSrc;
    QString minSrc;
    QString maxSrc;

    highSrc = "";
    highSrc.append(QString("%1 -").arg(histSrc.high));
    ui->labelHoogSrc->setText(highSrc);
    highSrc = "";
    highSrc.append(QString("%1 -").arg(histSrc.high/2));
    ui->labelMiddelSrc->setText(highSrc);
    meanSrc = "Mean: ";
    meanSrc.append(QString("%1").arg(histSrc.mean));
    ui->labelMeanSrc->setText(meanSrc);
    minSrc = "MinVal: ";
    minSrc.append(QString("%1").arg(histSrc.min));
    ui->labelMinSrc->setText(minSrc);
    maxSrc = "MaxVal: ";
    maxSrc.append(QString("%1").arg(histSrc.max));
    ui->labelMaxSrc->setText(maxSrc);
}

void MainWindow::DrawHistogramInfoDst() // laat juiste informatie zien in de labels die bij de histogram van de dst hoort.
{
    QString highDst;
    QString meanDst;
    QString minDst;
    QString maxDst;

    highDst = "";
    highDst.append(QString("%1 -").arg(histDst.high));
    ui->labelHoogDst->setText(highDst);
    highDst = "";
    highDst.append(QString("%1 -").arg(histDst.high/2));
    ui->labelMiddelDst->setText(highDst);
    meanDst = "Mean: ";
    meanDst.append(QString("%1").arg(histDst.mean));
    ui->labelMeanDst->setText(meanDst);
    minDst = "MinVal: ";
    minDst.append(QString("%1").arg(histDst.min));
    ui->labelMinDst->setText(minDst);
    maxDst = "MaxVal: ";
    maxDst.append(QString("%1").arg(histDst.max));
    ui->labelMaxDst->setText(maxDst);
}

void MainWindow::DrawHistogram(image_t *src, draw_hist_t *hist)
{
    int iaHist[256] = {0};
    int i, j;
    int teller  = 0, hoogte = 0, laagste = 255,
        hoogste = 0, divide = 1, gemiddelde = 0;

    // Loop through the image to make the histogram and extract
    // lowest & highest pixel value
    for(i = 0; i < src->height; i++)
    {
        for(j = 0; j < src->width; j++)
        {
            iaHist[src->data[i][j]] += 1;
            if(src->data[i][j] < laagste)
            {
                laagste = src->data[i][j];
            }
            if(src->data[i][j] > hoogste)
            {
                hoogste = src->data[i][j];
            }
        }
    }

    // Loop through histogram to determine height and mean value
    for(i = 0; i < 256; i++)
    {
        if(iaHist[i] > hoogte)
        {
            hoogte = iaHist[i];
        }
        gemiddelde += iaHist[i] * i;
        teller += iaHist[i];
    }

    gemiddelde = gemiddelde / teller;

    if((hoogte / 200) >= 1)
    {
        divide = hoogte / 200;
    }

    // Make an image of the histogram
    for(i = 0; i < 256; i++)
    {
        iaHist[i] = iaHist[i]/divide;
    }

    for(i = 0; i < hist->width; i++)
    {
        teller = 0;
        for(j = hist->height - 1; j >= 0; j--)
        {
            if(teller < iaHist[i])
            {
                hist->data[j][i] = 3;
            }
            else
            {
                hist->data[j][i] = 0;
            }
            teller++;
        }
    }

    hist->max  = hoogste;
    hist->min  = laagste;
    hist->high = hoogte;
    hist->mean = gemiddelde;
}

void MainWindow::mousePressEvent(QMouseEvent *event)
{
    int x, y;

    // Is the source image histogram displayed?
    if(ui->actionShow_histogram_src->isChecked())
    {
        // Yes, it is
        x = event->pos().x();
        y = event->pos().y();

        // Is the click event in the gv of the Histogram?
        if((x > 230) && (x < 487) && (y > 63) && (y < 264))
        {
            // Yes it is
            x -= 231;
            info.clear();
            ui->plainTextInfo->clear();

            // Left mouse button clicked?
            if(event->button() == Qt::LeftButton) // linker muisknop zal een threshold uitvoeren van 0 tot x positie
            {
                // Yes, threshold from 0 to x
                vThreshold(&imgSrc, &imgDst, 0, x);
                info.append(QString("1. Threshold 0 - %1\n").arg(x));
            }
            // Right mouse button clicked?
            else if(event->button() == Qt::RightButton)
            {
                // Yes, threshold from x to 255
                vThreshold(&imgSrc, &imgDst, x, 255);
                info.append(QString("1. Threshold %1 - 255\n").arg(x));
            }

            // Draw the image
            memcpy(&imgDstDisplayed, &imgDst, sizeof(image_t));
            DrawImage(ui->gvDst, &sceneDstImg, ui->LUTDst, &imgDstDisplayed);
            DrawHistogram(&imgDst, &histDst);
            DrawHistogramInfoDst();
            DrawHist(ui->gvHisDst, &sceneDstHist, &histDst);

            lut = LUT_BINARY;
            ui->LUTDst->setCurrentIndex(lut);

            ui->plainTextInfo->appendPlainText(info);
        }
    }
}

void MainWindow::on_actionOpen_triggered()
{
    if(devKit == NULL)
    {
        ui->actionClose->setEnabled(true);
        ui->actionOpen->setDisabled(true);

        devKit = new ft245app;
        devKit->move(850,450);
        devKit->show();

        connect(devKit, SIGNAL(destroyed()),this, SLOT(destroyedemitted()));

        connect(&(devKit->verbinding.object), SIGNAL(NewImage(int,image_t)), this, SLOT(NewImage(int,image_t)));
        connect(&(devKit->verbinding.object), SIGNAL(NewInfoStr(QString)), this, SLOT(NewInfo(QString)));

        setMode(RUNMODE_SNAPSHOT);
    }
}

void MainWindow::on_actionClose_triggered()
{
    if(devKit != NULL)
    {
        devKit->close();
    }

    setMode(RUNMODE_DEFAULT);
}

void MainWindow::destroyedemitted()
{
    ui->actionClose->setDisabled(true);
    ui->actionOpen->setEnabled(true);

    devKit = NULL;

    setMode(RUNMODE_DEFAULT);
}

void MainWindow::NewImage(int nr, image_t img)
{
    nr=nr;

    switch(runmode)
    {
        // --------------------------------------------------------------------
        case RUNMODE_DEFAULT:
        {
            ; // Do nothing
        }break;

        // --------------------------------------------------------------------
        // Receive snapshot from devboard and display as source image
        // Execute visionset so the destinition image is calculated by the
        // PC vision operators
        case RUNMODE_SNAPSHOT:
        {
            memcpy(&imgSrc, &img, sizeof(image_t));

            DrawImage(ui->gvSrc, &sceneSrcImg, ui->LUTSrc, &imgSrc);
            DrawHistogram(&imgSrc, &histSrc);
            DrawHistogramInfoSrc();
            DrawHist(ui->gvHisSrc, &sceneSrcHist, &histSrc);

            executeVisionSet();
        }break;

        // --------------------------------------------------------------------
        // Vision operations are executed on the devboard, so this is the
        // destination image
        case RUNMODE_CONTINUOUS:
        case RUNMODE_FILE_TO_DEVBOARD:
        {
            memcpy(&imgDst, &img, sizeof(image_t));

            // Set LUT for destination image
            ui->LUTDst->setCurrentIndex(img.lut);

            memcpy(&imgDstDisplayed, &imgDst, sizeof(image_t));
            DrawImage(ui->gvDst, &sceneDstImg, ui->LUTDst, &imgDstDisplayed);
            DrawHistogram(&imgDst, &histDst);
            DrawHistogramInfoDst();
            DrawHist(ui->gvHisDst, &sceneDstHist, &histDst);
        }break;

        // --------------------------------------------------------------------
        default:
        {
            ; // Do nothing
        }
    }
}

void MainWindow::NewInfo(QString str)
{
    switch(runmode)
    {
        // --------------------------------------------------------------------
        case RUNMODE_DEFAULT:
        case RUNMODE_SNAPSHOT:
        {
            ; // Do nothing
        }break;

        // --------------------------------------------------------------------
        case RUNMODE_CONTINUOUS:
        case RUNMODE_FILE_TO_DEVBOARD:
        {
            ui->plainTextInfo->appendPlainText(str);
        }break;

        // --------------------------------------------------------------------
        default:
        {
            ; // Do nothing
        }
    }
}

void MainWindow::on_actionSnapshot_triggered()
{
    if(devKit != NULL)
    {
        devKit->verbinding.SetMode(Mode_None);
    }

    vErase(&imgSrc);
    DrawImage(ui->gvSrc, &sceneSrcImg, ui->LUTSrc, &imgSrc);
    DrawHistogram(&imgSrc, &histSrc);
    DrawHistogramInfoSrc();
    DrawHist(ui->gvHisSrc, &sceneSrcHist, &histSrc);

    vErase(&imgDst);
    memcpy(&imgDstDisplayed, &imgDst, sizeof(image_t));
    DrawImage(ui->gvDst, &sceneDstImg, ui->LUTDst, &imgDstDisplayed);
    DrawHistogram(&imgDst, &histDst);
    DrawHistogramInfoDst();
    DrawHist(ui->gvHisDst, &sceneDstHist, &histDst);

    setMode(RUNMODE_SNAPSHOT);
}

void MainWindow::on_actionFile_to_DevBoard_triggered()
{
    if(runmode != RUNMODE_FILE_TO_DEVBOARD)
    {
        if(devKit != NULL)
        {
            devKit->verbinding.SetMode(Mode_None);
        }

        vErase(&imgSrc);
        DrawImage(ui->gvSrc, &sceneSrcImg, ui->LUTSrc, &imgSrc);
        DrawHistogram(&imgSrc, &histSrc);
        DrawHistogramInfoSrc();
        DrawHist(ui->gvHisSrc, &sceneSrcHist, &histSrc);

        vErase(&imgDst);
        memcpy(&imgDstDisplayed, &imgDst, sizeof(image_t));
        DrawImage(ui->gvDst, &sceneDstImg, ui->LUTDst, &imgDstDisplayed);
        DrawHistogram(&imgDst, &histDst);
        DrawHistogramInfoDst();
        DrawHist(ui->gvHisDst, &sceneDstHist, &histDst);

        setMode(RUNMODE_FILE_TO_DEVBOARD);
    }
}

void MainWindow::on_actionContinuous_triggered()
{
    if(runmode != RUNMODE_CONTINUOUS)
    {
        ContinuousRunning = false;
        if(devKit != NULL)
        {
            devKit->verbinding.SetMode(Mode_None);
        }

        vErase(&imgSrc);
        DrawImage(ui->gvSrc, &sceneSrcImg, ui->LUTSrc, &imgSrc);
        DrawHistogram(&imgSrc, &histSrc);
        DrawHistogramInfoSrc();
        DrawHist(ui->gvHisSrc, &sceneSrcHist, &histSrc);

        vErase(&imgDst);
        memcpy(&imgDstDisplayed, &imgDst, sizeof(image_t));
        DrawImage(ui->gvDst, &sceneDstImg, ui->LUTDst, &imgDstDisplayed);
        DrawHistogram(&imgDst, &histDst);
        DrawHistogramInfoDst();
        DrawHist(ui->gvHisDst, &sceneDstHist, &histDst);

        setMode(RUNMODE_CONTINUOUS);

        // Force button press openImageFile to start running live
        openImageFile();
    }
}


void MainWindow::setMode(runmode_t m)
{
    runmode = m;

    switch(runmode)
    {
        // --------------------------------------------------------------------
        case RUNMODE_DEFAULT:
        {
            ui->statusBar->showMessage(QString("Mode DEFAULT: file processed with Qt operators"));

            ui->actionSnapshot->setChecked(0);
            ui->actionContinuous->setChecked(0);
            ui->actionFile_to_DevBoard->setChecked(0);
            ui->pbOpenImage->setText(QString("Open Image"));
            ui->actionSnapshot->setEnabled(0);
            ui->actionContinuous->setEnabled(0);
            ui->actionFile_to_DevBoard->setEnabled(0);

            ui->pushButtonSingle->setVisible(0);
            ui->pbOpenImage->setFixedWidth(181);

            ui->spinBoxDevKit->setVisible(1);
            ui->labelImgNr->setVisible(1);
            ui->Operator->setFixedWidth(131);

            ui->gvSrc->setVisible(1);
            ui->labelSrcImageFromEVDK->setVisible(0);

            QStringList list;
            list.append("Qt Vision set 1"); // 0
            list.append("Qt Vision set 2"); // 1
            list.append("Qt Vision set 3"); // 2
            list.append("Qt Vision set 4"); // 3
            list.append("Qt Vision set 5"); // 4
            list.append("Qt Vision set 6"); // 5
            list.append("Qt Vision set 7"); // 6
            list.append("Qt Vision set 8"); // 7
            list.append("Qt Vision set 9"); // 8
            ui->Operator->clear();
            ui->Operator->addItems(list);
        }break;

        // --------------------------------------------------------------------
        case RUNMODE_SNAPSHOT:
        {
            ui->statusBar->showMessage(QString("Mode SNAPSHOT: snapshot from EVDK processed with Qt operators"));

            ui->actionSnapshot->setChecked(1);
            ui->actionContinuous->setChecked(0);
            ui->actionFile_to_DevBoard->setChecked(0);
            ui->pbOpenImage->setText(QString("Get snapshot from EVDK"));
            ui->actionSnapshot->setEnabled(1);
            ui->actionContinuous->setEnabled(1);
            ui->actionFile_to_DevBoard->setEnabled(1);

            ui->pushButtonSingle->setVisible(0);
            ui->pbOpenImage->setFixedWidth(181);

            ui->spinBoxDevKit->setVisible(1);
            ui->labelImgNr->setVisible(1);
            ui->Operator->setFixedWidth(131);

            ui->gvSrc->setVisible(1);
            ui->labelSrcImageFromEVDK->setVisible(0);

            QStringList list;
            list.append("Qt Vision set 1"); // 0
            list.append("Qt Vision set 2"); // 1
            list.append("Qt Vision set 3"); // 2
            list.append("Qt Vision set 4"); // 3
            list.append("Qt Vision set 5"); // 4
            list.append("Qt Vision set 6"); // 5
            list.append("Qt Vision set 7"); // 6
            list.append("Qt Vision set 8"); // 7
            list.append("Qt Vision set 9"); // 8
            ui->Operator->clear();
            ui->Operator->addItems(list);

        }break;

        // --------------------------------------------------------------------
        case RUNMODE_CONTINUOUS:
        {
            ui->statusBar->showMessage(QString("Mode CONTINUOUS: snapshot from EVDK processed with EVDK operators"));

            if(ContinuousRunning == true)
            {
                ui->pbOpenImage->setText(QString("Stop"));
            }
            else
            {
                ui->pbOpenImage->setText(QString("Continuous"));
            }

            ui->actionSnapshot->setChecked(0);
            ui->actionContinuous->setChecked(1);
            ui->actionFile_to_DevBoard->setChecked(0);

            ui->pushButtonSingle->setVisible(1);
            ui->pbOpenImage->setFixedWidth(101);
            ui->pushButtonSingle->setText(QString("Single"));

            ui->spinBoxDevKit->setVisible(1);
            ui->labelImgNr->setVisible(1);
            ui->Operator->setFixedWidth(131);

            // Hide source image, because there is only one image in continuous mode
            ui->gvSrc->setVisible(0);
            ui->labelSrcImageFromEVDK->setVisible(1);

            // hide histogram if visable
            if(ui->actionShow_histogram_src->isChecked())
            {
                ui->actionShow_histogram_src->setChecked(false);
                on_actionShow_histogram_src_triggered(ui->actionShow_histogram_src->isChecked());
            }

            QStringList list;
            list.append("EVDK Vision set 1"); // 0
            list.append("EVDK Vision set 2"); // 1
            list.append("EVDK Vision set 3"); // 2
            list.append("EVDK Vision set 4"); // 3
            list.append("EVDK Vision set 5"); // 4
            list.append("EVDK Vision set 6"); // 5
            list.append("EVDK Vision set 7"); // 6
            list.append("EVDK Vision set 8"); // 7
            list.append("EVDK Vision set 9"); // 8
            ui->Operator->clear();
            ui->Operator->addItems(list);

        }break;

        // --------------------------------------------------------------------
        case RUNMODE_FILE_TO_DEVBOARD:
        {
            ui->statusBar->showMessage(QString("Mode FILE_TO_DEVBOARD: file processed with EVDK operators"));

            ui->actionSnapshot->setChecked(0);
            ui->actionContinuous->setChecked(0);
            ui->actionFile_to_DevBoard->setChecked(1);
            ui->pbOpenImage->setText(QString("Open Image"));

            ui->pushButtonSingle->setVisible(1);
            ui->pbOpenImage->setFixedWidth(101);
            ui->pushButtonSingle->setText(QString("Send Again"));

            ui->spinBoxDevKit->setVisible(1);
            ui->labelImgNr->setVisible(1);
            ui->Operator->setFixedWidth(131);

            ui->gvSrc->setVisible(1);
            ui->labelSrcImageFromEVDK->setVisible(0);

            QStringList list;
            list.append("EVDK Vision set 1"); // 0
            list.append("EVDK Vision set 2"); // 1
            list.append("EVDK Vision set 3"); // 2
            list.append("EVDK Vision set 4"); // 3
            list.append("EVDK Vision set 5"); // 4
            list.append("EVDK Vision set 6"); // 5
            list.append("EVDK Vision set 7"); // 6
            list.append("EVDK Vision set 8"); // 7
            list.append("EVDK Vision set 9"); // 8
            ui->Operator->clear();
            ui->Operator->addItems(list);

        }break;

        // --------------------------------------------------------------------
        default:
        {
            ; // Do nothing
        }
    }
}

void MainWindow::closeEvent(QCloseEvent * event)
{
    event->accept();

    if(devKit != NULL)
    {
        devKit->close();
    }
}

void MainWindow::on_Operator_currentIndexChanged(int index)
{
    ui->spinBoxDevKit->setValue(1);

    if(devKit != NULL)
    {
        devKit->verbinding.SetImageNr(ui->spinBoxDevKit->value());
        devKit->verbinding.SetProgramma(index+1);
    }

    if(runmode == RUNMODE_DEFAULT || runmode == RUNMODE_SNAPSHOT)
    {
        executeVisionSet();
    }
}

void MainWindow::on_spinBoxDevKit_valueChanged(int index)
{
    if(devKit != NULL)
    {
        devKit->verbinding.SetImageNr(index);

        if(runmode == RUNMODE_SNAPSHOT)
        {
            devKit->verbinding.SetMode(Mode_None);
            ContinuousRunning = false;

            ui->plainTextInfo->clear();
            devKit->clearInfo();

            executeVisionSet();
        }
    }
    else
    {
        executeVisionSet();
    }
}

void MainWindow::on_pushButtonSingle_clicked()
{
    if(runmode == RUNMODE_CONTINUOUS)
    {
        devKit->verbinding.SetImageNr(ui->spinBoxDevKit->value());
        devKit->verbinding.SetProgramma(ui->Operator->currentIndex()+1);
        devKit->verbinding.SetMode(Mode_Single);

        ui->pbOpenImage->setText(QString("Continuous"));
        ui->plainTextInfo->clear();
        ContinuousRunning = false;
        devKit->clearInfo();
    }
    else if(runmode == RUNMODE_FILE_TO_DEVBOARD)
    {
        ui->plainTextInfo->clear();
        devKit->verbinding.SetImageNr(ui->spinBoxDevKit->value());
        devKit->verbinding.SetProgramma(ui->Operator->currentIndex()+1);
        devKit->verbinding.SetMode(Mode_Inject);
        devKit->clearInfo();
    }
}

void MainWindow::vShowDstImage()
{
    imgCount++;

    if((imgCount != 0) && (imgCount == ui->spinBoxDevKit->value()))
    {
        // Set LUT for destination image
        ui->LUTDst->setCurrentIndex(imgDst.lut);

        // Draw the image
        memcpy(&imgDstDisplayed, &imgDst, sizeof(image_t));
        DrawImage(ui->gvDst, &sceneDstImg, ui->LUTDst, &imgDstDisplayed);
        DrawHistogram(&imgDst, &histDst);
        DrawHistogramInfoDst();
        DrawHist(ui->gvHisDst, &sceneDstHist, &histDst);
    }
}
