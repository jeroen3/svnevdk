/******************************************************************************
 * Project    : Embedded Vision Design
 * Copyright  : 2012 HAN Embedded Systems Engineering
 * Author     : Sander
 *
 * Description: Header file for the D2xx interface
 *
 ******************************************************************************
  Change History:

    Version 1.0 - December 2011
    > Initial revision

******************************************************************************/
#ifndef D2XX_H
#define D2XX_H

#include <QObject>
#include <QThread>
#include <QTimer>
#include <windows.h>
#include "ftd2xx.h"
#include "operators.h"

// ----------------------------------------------------------------------------
// Type definitions
// ----------------------------------------------------------------------------
typedef enum
{
    d2xx_connected,
    d2xx_disconnected,
    d2xx_error
}d2xx_status_t;

typedef enum
{
    Mode_None = 0,
    Mode_Single,
    Mode_Continuous,
    Mode_Inject
} d2xx_mode_t;

// ----------------------------------------------------------------------------
// Classes
// ----------------------------------------------------------------------------
class d2xx_object: public QObject
{
    Q_OBJECT
public:
    d2xx_object(QObject *parent = 0);
    ~d2xx_object();

signals:
    void StatusUpdate(d2xx_status_t);
    void NewImage(int, image_t);
    void NewBenchmark(int,int,QString);
    void NewInfoStr(QString);

public slots:
    void SetImageNr(int);
    void SetMode(d2xx_mode_t);
    void SetProgramma(int);
    void InjectImage(image_t);

    void OpenDevice(void);
    void CloseDevice(void);
    void ReadFT(void);

private:
    FT_HANDLE ftHandle;
    QTimer * timer;
    image_t img_to_send;
};

class d2xx_manager: public QObject
{
    Q_OBJECT
public:
    explicit d2xx_manager(QObject *parent = 0);
    ~d2xx_manager();
    void OpenDevice(void);
    void CloseDevice(void);
    void SetMode(d2xx_mode_t);
    void SetImageNr(int);
    void SetProgramma(int);
    void InjectImage(image_t);

signals:
    void OpenDeviceSig(void);
    void CloseDeviceSig(void);
    void SetModeSig(d2xx_mode_t);
    void SetImageNrSig(int);
    void SetProgrammaSig(int);
    void InjectImageSig(image_t);

public slots:

public:
    d2xx_object object;

private:
    QThread thread;
};

#endif // D2XX_H
