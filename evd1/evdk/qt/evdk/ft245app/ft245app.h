/******************************************************************************
 * Project    : Embedded Vision Design
 * Copyright  : 2012 HAN Embedded Systems Engineering
 * Author     : Sander
 *
 * Description: Header file for the FT245 App
 *
 ******************************************************************************
  Change History:

    Version 1.0 - December 2011
    > Initial revision

******************************************************************************/
#ifndef FT245APP_H
#define FT245APP_H

#include <QCloseEvent>
#include <QMainWindow>
#include "windows.h"
#include "ftd2xx.h"
#include "d2xx.h"

namespace Ui {
    class ft245app;
}

// ----------------------------------------------------------------------------
// Classes
// ----------------------------------------------------------------------------
class ft245app : public QMainWindow
{
    Q_OBJECT

public:
    explicit ft245app(QWidget *parent = 0);
    ~ft245app();
    void clearInfo(void);
public slots:

    void StatusUpdate(d2xx_status_t);
    void AddBench(int, int, QString);

private slots:
    void on_bnt_connect_dev_clicked();
    void on_bnt_disconnect_dev_clicked();

public:
    d2xx_manager verbinding;

    void closeEvent(QCloseEvent *);

private:
    Ui::ft245app *ui;
    FT_STATUS ftStatus;
    DWORD numDevs;
    FT_DEVICE_LIST_INFO_NODE *devInfo;
};

#endif // FT245APP_H
