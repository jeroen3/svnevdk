/******************************************************************************
 * Project    : Embedded Vision Design
 * Copyright  : 2012 HAN Embedded Systems Engineering
 * Author     : Sander/Hugo
 *
 * Description: Implementation file for the D2xx interface
 *
 ******************************************************************************
  Change History:

    Version 2.0 - October 2012
    > Implemented new image sructure
    > Removed printf() and added qDebug() on errornous situations

    Version 1.0 - December 2011
    > Initial revision

******************************************************************************/
#include <QDebug>
#include <QMetaType>

#include "d2xx.h"

// ----------------------------------------------------------------------------
// Defines
// ----------------------------------------------------------------------------
// HELLO is used as a version number that must match the version number in
// the microcontroller software
#define HELLO        ('3')
#define EXIT         ('Q')
#define IMAGE        ('B')
#define BENCHMARK    ('C')
#define INFO         ('I')
#define SEND_IMAGE   ('S')

#define PROGRAMMA_NR ('D')
#define IMAGE_NR     ('E')
#define MODE         ('M')

// ----------------------------------------------------------------------------
// Global variables
// ----------------------------------------------------------------------------
uint8_t buffer_bench[24];
uint8_t buffer_info[200];
uint8_t buffer_img_header[9];
uint8_t buffer_send_img_header[9];

image_t buffer_img;

// ----------------------------------------------------------------------------
// Function implementations
// ----------------------------------------------------------------------------
d2xx_manager::d2xx_manager(QObject *parent) :
    QObject(parent)
{

    connect(this, SIGNAL(OpenDeviceSig()),&object, SLOT(OpenDevice()));
    connect(this, SIGNAL(CloseDeviceSig()),&object, SLOT(CloseDevice()));
    connect(this, SIGNAL(SetModeSig(d2xx_mode_t)),&object, SLOT(SetMode(d2xx_mode_t)));
    connect(this, SIGNAL(SetImageNrSig(int)),&object, SLOT(SetImageNr(int)));
    connect(this, SIGNAL(InjectImageSig(image_t)),&object, SLOT(InjectImage(image_t)));
    connect(this, SIGNAL(SetProgrammaSig(int)),&object,SLOT(SetProgramma(int)));

    object.moveToThread(&thread);

    thread.start(QThread::TimeCriticalPriority);
}

d2xx_manager::~d2xx_manager(void)
{
    thread.terminate();
}

void d2xx_manager::OpenDevice(void)
{
    emit OpenDeviceSig();
}

void d2xx_manager::SetMode(d2xx_mode_t mode)
{
    emit SetModeSig(mode);
}

void d2xx_manager::CloseDevice(void)
{
    emit CloseDeviceSig();
}

void d2xx_manager::SetImageNr(int nr)
{
    emit SetImageNrSig(nr);
}

void d2xx_manager::SetProgramma(int nr)
{
    emit SetProgrammaSig(nr);
}

void d2xx_manager::InjectImage(image_t img)
{
    emit InjectImageSig(img);
}

d2xx_object::d2xx_object(QObject *parent) :
    QObject(parent)
{
    ftHandle = NULL;
    timer = NULL;

    qRegisterMetaType<d2xx_status_t>("d2xx_status_t");
    qRegisterMetaType<image_t>("image_t");
    qRegisterMetaType<d2xx_mode_t>("d2xx_mode_t");
}

d2xx_object::~d2xx_object(void)
{
    CloseDevice();

    if(timer != NULL)
    {
        delete timer;
    }
}

void d2xx_object::OpenDevice(void)
{
    FT_STATUS ftStatus;
    DWORD numDevs;
    DWORD DevToOpen = 1000;

    ftStatus = FT_CreateDeviceInfoList(&numDevs);
    if (ftStatus != FT_OK)
    {
        emit StatusUpdate(d2xx_error);
        return;
    }

    if(numDevs > 0)
    {
        FT_DEVICE_LIST_INFO_NODE *devInfo;

        // Allocate storage for list based on numDevs
        devInfo = (FT_DEVICE_LIST_INFO_NODE*)malloc(sizeof(FT_DEVICE_LIST_INFO_NODE)*numDevs);

        // Get the device information list
        ftStatus = FT_GetDeviceInfoList(devInfo,&numDevs);
        if (ftStatus == FT_OK)
        {
            for (uint i = 0; i < numDevs; i++)
            {
                if(strcmp(devInfo[i].Description, "EMBEDDED VISION DEVBOARD") == 0)
                {
                    DevToOpen = i;
                }
                else
                {
                    qDebug() << "D2xx: devInfo =" << devInfo[i].Description;
                }
            }
        }
        free(devInfo);
    }
    else
    {
        qDebug() << "D2xx: No D2xx enabled devices found";
        emit StatusUpdate(d2xx_error);
        return;
    }

    if(DevToOpen != 1000)
    {
        ftStatus = FT_Open(DevToOpen, &ftHandle);
    }
    else
    {
        ftStatus = FT_DEVICE_NOT_FOUND;
    }


    if(ftStatus == FT_OK)
    {

        ftStatus = FT_SetTimeouts(ftHandle, FT_DEFAULT_RX_TIMEOUT, FT_DEFAULT_TX_TIMEOUT);
        if (ftStatus != FT_OK)
        {
            CloseDevice();
            return;
        }

        ftStatus = FT_SetBitMode(ftHandle, 0xff, FT_BITMODE_SYNC_FIFO);
        if (ftStatus != FT_OK)
        {
            CloseDevice();
            return;
        }

        FT_Purge(ftHandle, FT_PURGE_RX | FT_PURGE_TX);


        uint8_t TxBuffer[1];
        DWORD BytesWritten;
        TxBuffer[0] = HELLO;

        ftStatus = FT_Write(ftHandle, TxBuffer, 1, &BytesWritten);
        if (ftStatus != FT_OK)
        {
            qDebug() << "D2xx: Write error\n";
            CloseDevice();
            return;
        }

        uint8_t RxBuffer;
        DWORD BytesReceived;


        ftStatus = FT_Read(ftHandle, &RxBuffer, 1, &BytesReceived);

        if(ftStatus != FT_OK || RxBuffer != HELLO || BytesReceived != 1)
        {
            qDebug() << "D2xx: Read error";
            qDebug() << "D2xx: Check version of microcontroller software, PC version is" << HELLO;
            CloseDevice();
            return;
        }

        if(timer == NULL)
        {
            timer = new QTimer;
            timer->setSingleShot(true);
        }

        connect(timer, SIGNAL(timeout()), this, SLOT(ReadFT()));
        timer->start(50);

        emit StatusUpdate(d2xx_connected);

    }
    else
    {
        qDebug() << "D2xx: Open Failed";
        emit StatusUpdate(d2xx_error);
    }
}

void d2xx_object::CloseDevice(void)
{
    if(timer != NULL)
    {
        disconnect(timer, SIGNAL(timeout()), this, SLOT(ReadFT()));
    }

    if(ftHandle != NULL)
    {
        uint8_t TxBuffer[1];
        DWORD BytesWritten;
        TxBuffer[0] = EXIT;

        FT_Write(ftHandle, TxBuffer, 1, &BytesWritten);

        FT_Close(ftHandle);
        ftHandle = NULL;
        emit StatusUpdate(d2xx_disconnected);
    }
}

void d2xx_object::ReadFT(void)
{
    if(ftHandle != NULL)
    {
        bool loop = true;
        while(loop == true)
        {
            loop = false;
            uint8_t buffer;
            DWORD BytesReceived;
            DWORD BytesWritten;
            FT_STATUS ftStatus;

            ftStatus = FT_Read(ftHandle,&buffer,1,&BytesReceived);
            if(ftStatus != FT_OK)
            {
                CloseDevice();
                return;
            }

            if(BytesReceived == 1)
            {
                //loop = true;
                switch(buffer)
                {
                case BENCHMARK:
                    ftStatus = FT_Read(ftHandle,buffer_bench,24,&BytesReceived);
                    if(ftStatus != FT_OK)
                    {
                        CloseDevice();
                        return;
                    }

                    emit NewBenchmark(*((uint32_t *)buffer_bench),*((uint32_t *)&buffer_bench[4]) ,QString((char *)&buffer_bench[8]));
                    break;
                case IMAGE:
                    ftStatus = FT_Read(ftHandle,buffer_img_header,9,&BytesReceived);
                    if(ftStatus != FT_OK)
                    {
                        CloseDevice();
                        return;
                    }

                    ftStatus = FT_Read(ftHandle,buffer_img.data,sizeof(buffer_img.data),&BytesReceived);
                    if(ftStatus != FT_OK)
                    {
                        CloseDevice();
                        return;
                    }

                    buffer_img.width = *((uint16_t *)&buffer_img_header[1]);
                    buffer_img.height = *((uint16_t *)&buffer_img_header[3]);
                    buffer_img.lut = *((uint16_t *)&buffer_img_header[5]);

                    if(buffer_img.height != 0 && buffer_img.width != 0)
                    {
                        emit NewImage(buffer_img_header[0], buffer_img);
                    }
                    break;
                case INFO:
                    ftStatus = FT_Read(ftHandle,buffer_info,200,&BytesReceived);
                    if(ftStatus != FT_OK)
                    {
                        CloseDevice();
                        return;
                    }

                    emit NewInfoStr(QString((char *)buffer_info));

                    break;
                case SEND_IMAGE:
                    buffer_send_img_header[0] = SEND_IMAGE;
                    uint16_t * p;
                    p = (uint16_t *)&buffer_send_img_header[1];
                    *p = img_to_send.width;

                    p = (uint16_t *)&buffer_send_img_header[3];
                    *p = img_to_send.height;

                    p = (uint16_t *)&buffer_send_img_header[5];
                    *p = img_to_send.lut;

                    FT_Write(ftHandle, buffer_send_img_header, 9, &BytesWritten);
                    FT_Write(ftHandle, img_to_send.data, sizeof(img_to_send.data), &BytesWritten);

                    break;
                }
            }
        }

        timer->start(0);
    }
}

void d2xx_object::SetMode(d2xx_mode_t mode)
{
    if(ftHandle != NULL)
    {
        uint8_t TxBuffer[2];
        DWORD BytesWritten;
        TxBuffer[0] = MODE;
        TxBuffer[1] = (int)mode;

        FT_Write(ftHandle, TxBuffer, 2, &BytesWritten);
    }
}

void d2xx_object::SetImageNr(int nr)
{
    if(ftHandle != NULL)
    {
        uint8_t TxBuffer[2];
        DWORD BytesWritten;
        TxBuffer[0] = IMAGE_NR;
        TxBuffer[1] = nr;

        FT_Write(ftHandle, TxBuffer, 2, &BytesWritten);
    }
}

 void d2xx_object::SetProgramma(int nr)
 {
     if(ftHandle != NULL)
     {
         uint8_t TxBuffer[2];
         DWORD BytesWritten;
         TxBuffer[0] = PROGRAMMA_NR;
         TxBuffer[1] = nr;

         FT_Write(ftHandle, TxBuffer, 2, &BytesWritten);
     }
 }

 void d2xx_object::InjectImage(image_t img)
 {
     memcpy(&img_to_send,&img,sizeof(image_t));
 }

