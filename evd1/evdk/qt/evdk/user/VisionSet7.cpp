#include "VisionSet.h"

void VisionSet7(MainWindow *mw, image_t *pSrc, image_t *pDst)
{
    // Contrast stretch the source image, result in destination image
    vCopy(pSrc,pDst);
    vErase(pDst);
    uint32_t teller=0;
    for(uint32_t y=pDst->height; y; y--){
        for(uint32_t x=pDst->width; x; x--){
            pDst->data[y][x] = teller++;
        }
    }

    pDst->lut = LUT_CLIP;
    mw->vShowDstImage();
    info.append(QString("1. incfill for\n"));

    vErase(pDst);
    pDst->lut = LUT_CLIP;
    mw->vShowDstImage();
    info.append(QString("2. erase \n"));

    uint32_t i = pDst->height*pDst->width;
    uint8_t *data = &pDst->data[0][0];
    while(i--){
        *data++ = teller++;
    }

    pDst->lut = LUT_CLIP;
    mw->vShowDstImage();
    info.append(QString("2. incfill while\n"));
}
