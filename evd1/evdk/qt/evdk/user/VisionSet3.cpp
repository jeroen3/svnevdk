#include "VisionSet.h"

void VisionSet3(MainWindow *mw, image_t *pSrc, image_t *pDst)
{
    // Contrast stretch the source image, result in destination image
    vCopy(pSrc, pDst);
    pDst->lut = LUT_CLIP;
    mw->vShowDstImage();
    info.append(QString("1. Source\n"));

    // Notice that from now on the destination image also is the source image!!

    vAdd(pSrc, pDst);
    pDst->lut = LUT_CLIP;
    mw->vShowDstImage();
    info.append(QString("2. vAdd\n"));

    vThresholdIsoData(pSrc, pDst, BRIGHT);
    pDst->lut = LUT_BINARY;
    mw->vShowDstImage();
    info.append(QString("3. vThreshold\n"));

    vMultiply(pSrc, pDst);
    pDst->lut = LUT_CLIP;
    mw->vShowDstImage();
    info.append(QString("4. vAdd\n"));

    vErase(pDst);
    pDst->lut = LUT_CLIP;
    mw->vShowDstImage();
    info.append(QString("5. vErase\n"));

    vSetBorders(pDst,pDst,127);
    pDst->lut = LUT_CLIP;
    mw->vShowDstImage();
    info.append(QString("6. vSetBorders\n"));

}


