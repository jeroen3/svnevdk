#include "VisionSet.h"

void VisionSet5(MainWindow *mw, image_t *pSrc, image_t *pDst)
{
    // Contrast stretch the source image, result in destination image

    vContrastStretchFast(pSrc, pDst);
    pDst->lut = LUT_STRETCH;
    mw->vShowDstImage();
    info.append(QString("1. vContrastStretchFast\n"));

    vThresholdIsoData(pSrc,pDst,DARK);

    // Notice that from now on the destination image also is the source image!!

    vFillHoles(pDst, pDst, EIGHT);
    pDst->lut = LUT_LABELED;
    mw->vShowDstImage();
    info.append(QString("2. vFillHoles4\n"));

    vFindEdges(pDst, pDst, EIGHT);
    pDst->lut = LUT_LABELED;
    mw->vShowDstImage();
    info.append(QString("3. vFindEdges"));
}
