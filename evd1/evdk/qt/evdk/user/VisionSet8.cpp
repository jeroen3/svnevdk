#include "VisionSet.h"

void VisionSet8(MainWindow *mw, image_t *pSrc, image_t *pDst)
{
    // Contrast stretch the source image, result in destination image

    vContrastStretchFast(pSrc, pDst);
    pDst->lut = LUT_STRETCH;
    mw->vShowDstImage();
    info.append(QString("1. vContrastStretchFast\n"));

    // Notice that from now on the destination image also is the source image!!

    vThreshold(pDst, pDst, 0, 100);
    pDst->lut = LUT_BINARY;
    mw->vShowDstImage();
    info.append(QString("2. vThreshold 0-100\n"));
}
