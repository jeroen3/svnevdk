#include "VisionSet.h"
#include "extra_operators.h"

void VisionSet1(MainWindow *mw, image_t *pSrc, image_t *pDst)
{
    /* VisionSet for segments.png */
    static int bcd[10];
    int i;
    // Contrast stretch the source image, result in destination image

    vContrastStretchFast(pSrc, pDst);
    pDst->lut = LUT_STRETCH;
    //mw->vShowDstImage();
    info.append(QString("1. vContrastStretchFast\n"));

    // Notice that from now on the destination image also is the source image!!

    vThresholdIsoData(pDst, pDst, DARK);
    pDst->lut = LUT_BINARY;
    mw->vShowDstImage();
    info.append(QString("2. vThresholdIsoData"));

    image_roi_t pDstRoi;
    pDstRoi.h = 32;
    pDstRoi.w = 32;
    pDstRoi.x = 0;
    pDstRoi.y = 0;
    pDstRoi = sRoiValidate(pDstRoi);
    QDEBUG("Visionset 1 - - - - - - - - - - - - ");

    pDst->lut = LUT_LABELED;

    for(i=0; i<5; i++){
        QDEBUG(" ");
        QDEBUG("Digit "<<i);
        bcd[i] = imageToBCD(pDst,pDstRoi);
        pDstRoi.x += 32;
        mw->vShowDstImage();
    }
    // Next line
    pDstRoi.y += 32;
    pDstRoi.x = 0;
    for(i=5; i<10; i++){
        QDEBUG(" ");
        QDEBUG("Digit "<<i);
        bcd[i] = imageToBCD(pDst,pDstRoi);
        pDstRoi.x += 32;
        mw->vShowDstImage();
    }

    info.append(QString("3. imageToBCD"));
    for(i=0; i<10; i++){
         QDEBUG("bcd's "<< i << ": " <<bcd[i]);
         info.append(QString("Verwacht: "));
         info.append(QString::number(i));
         info.append(QString(" Gevonden: "));
         info.append(QString::number(bcd[i]));
         info.append(QString("\n"));
    }

}

/*for(x=pDstRoi.x; x<pDstRoi.w+pDstRoi.x; x++){
    pDst->data[pDstRoi.y][x] = i+1;
}
for(y=pDstRoi.y; y<pDstRoi.h+pDstRoi.y; y++){
    pDst->data[y][pDstRoi.x] = i+1;
}*/
