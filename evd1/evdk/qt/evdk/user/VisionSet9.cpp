
#include "VisionSet.h"

void VisionSet9(MainWindow *mw, image_t *pSrc, image_t *pDst)
{
    // Contrast stretch the source image, result in destination image

    vContrastStretchFast(pSrc, pDst);
    vThresholdOtsu(pSrc, pDst,DARK);
    vRemoveBorderBlobs(pDst,pDst,EIGHT);
    pDst->lut = LUT_BINARY;
    mw->vShowDstImage();
    info.append(QString("1. vContrastStretchFast\n"));
    info.append(QString("1. vThresHoldIso\n"));

    // Notice that from now on the destination image also is the source image!!

    uint8_t blobcount = iLabelBlobs(pDst, pDst, EIGHT);
    pDst->lut = LUT_LABELED;
    mw->vShowDstImage();
    if(blobcount != 0xFF){
        info.append(QString("2. iLabelBlobs\n"));

        blobinfo_t blobs[254] ;
        vBlobAnalyse(pDst,blobcount,blobs);
        info.append(QString("-. vBlobAnalyse\n"));
/*
        for(int i=0; i<blobcount; i++){
            blobs[i].perimeter = blobs[i].perimeter*blobs[i].perimeter / (double)blobs[i].nof_pixels;
        }
*/
        for(int i=0; i<blobcount; i++){
            float formfactor = (blobs[i].perimeter*blobs[i].perimeter / (double)blobs[i].nof_pixels);
            info.append(QString("BLOB "));
            info.append(QString::number(i));
            info.append(QString(": W="));
            info.append(QString::number(blobs[i].width));
            info.append(QString("\nBLOB "));
            info.append(QString::number(i));
            info.append(QString(": H="));
            info.append(QString::number(blobs[i].height));
            info.append(QString("\nBLOB "));
            info.append(QString::number(i));
            info.append(QString(": N="));
            info.append(QString::number(blobs[i].nof_pixels));
            info.append(QString("\nBLOB "));
            info.append(QString::number(i));
            info.append(QString(": P="));
            info.append(QString::number(blobs[i].perimeter));
            info.append(QString("\nBLOB "));
            info.append(QString::number(i));
            info.append(QString(": F="));
            info.append(QString::number(formfactor));
            info.append(QString("\nBLOB "));
            info.append(QString::number(i));
            // Formfactor compare
            if(formfactor > 19.0){
                info.append(QString(": Triangle"));;
            }else
                if(formfactor > 15.0){
                    info.append(QString(": Square"));
                }else
                    if(formfactor > 13.0){
                        info.append(QString(": Circle"));
                    }else{
                        info.append(QString(": Unknown"));
                    }
            info.append(QString("\n"));
        }

        for(int i=0; i<blobcount; i++){
            QDEBUG("BLOB "<< i << ": W=" << blobs[i].width<<" H=" << blobs[i].height<<" N=" << blobs[i].nof_pixels<<" P=" << blobs[i].perimeter );
        }
    }else{
        info.append(QString("2. iLabelBlobs\nErr: label overflow\n"));
    }
}
