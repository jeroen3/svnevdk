/******************************************************************************
 * Project    : Embedded Vision Design
 * Copyright  : 2012 HAN Embedded Systems Engineering
 * Author     : EVD Class 2013-2014
 *
 * Description: Implementation of image operators
 *              Functions from other people
 *
 ******************************************************************************
  Change History:

    Version 2.0 - October 2012
    > Implemented new image structure

    Version 1.0 - September 2011
    > Initial revision

******************************************************************************/
#ifdef __arm__
    #include "stm32f4xx.h"
#endif
#ifdef __QT__
    #include <QElapsedTimer>
    #include <QtEndian>
#endif
#include "operators.h"
#include "extra_operators.h"
#include <stdlib.h>
#include "math.h"


// ----------------------------------------------------------------------------
// vDrawLine
// ----------------------------------------------------------------------------
void vDrawLine (image_t *img, const point_t start, const point_t end, const uint8_t size, const uint8_t value)
{
    // http://en.wikipedia.org/wiki/Bresenham's_line_algorithm
    // Teken lijn van start x,y -> end x,y, met dikte size en kleurwaarde value

    const int32_t dX    = abs(end.x - start.x);             // Delta X
    const int32_t dY    = abs(end.y - start.y);             // Delta Y
    const int32_t sX    = (start.x < end.x) ? (1) : (-1);   // Richting X
    const int32_t sY    = (start.y < end.y) ? (1) : (-1);   // Richting Y
    const uint8_t width = (size - 1) / 2;

    uint32_t x  = start.x;
    uint32_t y  = start.y;
    int32_t err = (dX > dY ? dX : -dY) / 2;
    int32_t e2  = 0;
    uint8_t i   = 0;

    // Debug
    //QDEBUG("> vDrawLine: Begin, size = " << size << " with value = " << value << " from " << start.x << "." << start.y << " to " << end.x << "." << end.y );

    // Teken lijn
    while( !(x == end.x && y == end.y) )
    {
        e2 = err;

        // Teken pixel
        img->data[y][x] = value;

        // Teken rondom
        for ( i = -width; i <= width; i++ )
        {
            // Four
            img->data[y][x+i]   = value;
            img->data[y][x-i]   = value;
            img->data[y+i][x]   = value;
            img->data[y-i][x]   = value;

            // Eight
            img->data[y+i][x+i] = value;
            img->data[y+i][x-i] = value;
            img->data[y-i][x+i] = value;
            img->data[y-i][x-i] = value;
        }

        if (e2 > -dX) { err -= dY; x += sX; } // Volgende X
        if (e2 <  dY) { err += dX; y += sY; } // Volgende Y
    }

    // Debug info
    //QDEBUG("> Done!" );
}

void vRectangleRoi(image_t *img, image_roi_t roi, uint8_t color ){
    /* tl - tr
     * |    |
     * bl - br */
    point_t tr = {0,0},tl= {0,0},br= {0,0},bl= {0,0};
    tl.x = roi.x;
    tl.y = roi.y;
    tr.x = roi.x + roi.w;
    tr.y = roi.y;
    bl.x = roi.x;
    bl.y = roi.y + roi.h;
    br.x = roi.x + roi.w;
    br.y = roi.y + roi.h;
    vDrawLine(img,tl,tr,1,color);
    vDrawLine(img,tl,bl,1,color);
    vDrawLine(img,br,tr,1,color);
    vDrawLine(img,br,bl,1,color);
}

