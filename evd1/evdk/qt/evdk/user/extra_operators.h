#ifndef EXTRA_OPERATORS_H
#define EXTRA_OPERATORS_H

#include "stdint.h"

// Drawline, Stephan Martens
typedef struct
{
    uint8_t x;
    uint8_t y;
} point_t;

void vDrawLine (image_t *img, const point_t start, const point_t end, const uint8_t size, const uint8_t value);

void vRectangleRoi(image_t *pDst, image_roi_t roi, uint8_t color);


#endif // EXTRA_OPERATORS_H
