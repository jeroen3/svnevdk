#include "VisionSet.h"
#include <QElapsedTimer>


void VisionSet6(MainWindow *mw, image_t *pSrc, image_t *pDst)
{
    // Contrast stretch the source image, result in destination image
    QElapsedTimer timer;
    qint64 nanoSec;
    timer.start();

    timer.restart();
        vCopy(pSrc, pDst);
    nanoSec = timer.nsecsElapsed();
        pDst->lut = LUT_CLIP;
        mw->vShowDstImage();
        info.append(QString("1. vCopy\n"));
    //QDEBUG("1: "<<nanoSec);

    timer.restart();
        vRotate180(pDst);
        pDst->lut = LUT_CLIP;
        mw->vShowDstImage();
        info.append(QString("2. vRotate180\n"));
    nanoSec = timer.nsecsElapsed();
    //QDEBUG("2: "<<nanoSec);

    timer.restart();
        vContrastStretch(pSrc, pDst,0,255);
    nanoSec = timer.nsecsElapsed() / 1000;
        pDst->lut = LUT_STRETCH;
        mw->vShowDstImage();
        info.append(QString("3. vContrastStretch\n"));
    //QDEBUG("3: "<<nanoSec);

    timer.restart();
        vThreshold(pDst,pDst,200,255);
    nanoSec = timer.nsecsElapsed();
        pDst->lut = LUT_BINARY;
        mw->vShowDstImage();
        info.append(QString("4. vThreshold\n"));
    //QDEBUG("4: "<<nanoSec);

    timer.restart();
        vErase(pDst);
    nanoSec = timer.nsecsElapsed();
        pDst->lut = LUT_CLIP;
        mw->vShowDstImage();
        info.append(QString("5. vErase"));
    //QDEBUG("5: "<<nanoSec);


}

