#include "VisionSet.h"
#include <QElapsedTimer>
#include "extra_operators.h"

void VisionSet2(MainWindow *mw, image_t *pSrc, image_t *pDst)
{
    /* VisionSet for 3 digit segments display */
    // Contrast stretch the source image, result in destination image
    /* VisionSet for segments.png */
    static int bcd[4];
    int i;
    // Contrast stretch the source image, result in destination image

    //vContrastStretchFast(pSrc, pDst);
    vCopy(pSrc,pDst);
    pDst->lut = LUT_STRETCH;
    //mw->vShowDstImage();
    info.append(QString("1. vContrastStretchFast\n"));

    // Notice that from now on the destination image also is the source image!!

    vThresholdIsoData(pDst, pDst, BRIGHT);
    pDst->lut = LUT_BINARY;
    mw->vShowDstImage();
    info.append(QString("2. vThresholdIsoData"));

    image_roi_t pDstRoi;
    pDstRoi.h = 40;
    pDstRoi.w = 24;
    pDstRoi.x = 40;
    pDstRoi.y = 64;

    pDst->lut = LUT_LABELED;
    for(i=0; i<3; i++){
        QDEBUG(" ");
        QDEBUG("Digit "<<i);
        bcd[i] = imageToBCD(pDst,pDstRoi);
       // vRectangleRoi(pDst,pDstRoi,2);
        pDstRoi.x += pDstRoi.w;
        //mw->vShowDstImage();
    }

    // Draw roi
    pDstRoi.h = 40;
    pDstRoi.w = 24;
    pDstRoi.x = 40;
    pDstRoi.y = 64;
    info.append(QString("3. imageToBCD"));
    for(i=0; i<3; i++){
        QDEBUG("bcd's "<< i << ": " <<bcd[i]);
        vRectangleRoi(pDst,pDstRoi,2);
        pDstRoi.x += pDstRoi.w;
    }
    mw->vShowDstImage();

}

