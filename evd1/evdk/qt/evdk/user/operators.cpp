/******************************************************************************
 * Project    : Embedded Vision Design
 * Copyright  : 2012 HAN Embedded Systems Engineering
 * Author     : Hugo
 *
 * Description: Implementation of image operators
 *              Implement these functions only using C (not C++)!!
 *
 ******************************************************************************
  Change History:

    Version 2.0 - October 2012
    > Implemented new image structure

    Version 1.0 - September 2011
    > Initial revision

******************************************************************************/
#ifdef __arm__
#include "stm32f4xx.h"
#endif
#ifdef __QT__
#include <QElapsedTimer>
#include <QtEndian>
#endif
#include "operators.h"
#include "math.h"
#include "extra_operators.h"

#define vContrastStretch_with_lut 1

// ----------------------------------------------------------------------------
// Function implementations
// ----------------------------------------------------------------------------
void vContrastStretch(image_t *src, // must be a greyscale image
                      image_t *dst, // result is a greyscale image
                      uint16_t bottom,
                      uint16_t top)
{
    const uint32_t aantal_pixels = src->width * src->height;
    uint8_t lpixel = 0xFF;
    uint8_t hpixel = 0;
#if vContrastStretch_with_lut
    uint8_t lut[0xFF+1];
#endif
    // Step 1, find lpixel and hpixel
    {
        uint32_t i, limit = aantal_pixels;
        uint8_t *src_data = &src->data[0][0] -1;
        for(i=0; i<limit; i++){
            // Unroll 1
            if( *++src_data < lpixel ){
                lpixel = *src_data;
            }
            if( *src_data > hpixel ){
                hpixel = *src_data;
            }
        }
    }

    // Check for exception
    if( lpixel == hpixel ){
        // Image is equally colored
        uint32_t i;
        uint8_t *dst_data = &dst->data[0][0];
        uint8_t center = ((bottom+top)/2);
        for( i = aantal_pixels/4; i ; i--){
            *dst_data++ = center;
            *dst_data++ = center;
            *dst_data++ = center;
            *dst_data++ = center;
        }
        // Skip last part
        return;
    }

#if vContrastStretch_with_lut
    // Step 3: Calculate stretching table
    {
        uint8_t *plut = lut;
        uint32_t i = sizeof(lut), x=0;
        float stretch  = ((float)(top-bottom)/(float)(hpixel-lpixel));
        float rounding = (0.5f + (float)bottom);
        while(i--){
            float t = x++ - lpixel;
            t *= stretch;
            t += rounding;
            *plut++ = t;
        }
    }
    // Step 4: Stretch image
    {
        // Pointers
        uint8_t* src_data = &src->data[0][0];   // Reset pointer
        uint8_t* dst_data = &dst->data[0][0];   // Reset pointer
        // Assign
        uint32_t i = aantal_pixels/4;
        while(i--){
            *dst_data++ = lut[*src_data++];
            *dst_data++ = lut[*src_data++];
            *dst_data++ = lut[*src_data++];
            *dst_data++ = lut[*src_data++];
        }
    }
#else
    // Stretch image
    {
        uint32_t i;
        // Pointers
        uint8_t* src_data = &src->data[0][0];   // Reset pointer
        uint8_t* dst_data = &dst->data[0][0];   // Reset pointer
        // Values
        float stretch  = ((float)(top-bottom)/(float)(hpixel-lpixel));
        float rounding = (0.5f + (float)bottom);
        // Execute stretch, note QSUB8 improvement -lpixel
        for(i=0; i<aantal_pixels; i++){
            float t = *src_data++ - lpixel;
            t *= stretch;
            t += rounding;
            *dst_data++ = t;
        }
    }

#endif

}



// ----------------------------------------------------------------------------
void vThreshold(image_t *src,
                image_t *dst, // result is a binary image
                uint8_t low,
                uint8_t high)
{
    uint32_t aantal_pixels = src->width * dst->height;
    uint8_t lut[0xFF+1];
    // Calculate threshold table
    {
        uint8_t *plut = &lut[0xFF];
        uint32_t i = sizeof(lut);
        while(i--){
            (i <  low || i > high) ? ( *plut-- = 0 ):( *plut-- = 1 );
        }
    }
    // Apply threshold table
    {
        uint8_t *src_data = &src->data[0][0];   // Set pointer
        uint8_t *dst_data = &dst->data[0][0];
        uint32_t i = aantal_pixels;
        while(i--){
            *dst_data++ = lut[*src_data++];
        }
    }
}

// ----------------------------------------------------------------------------
void vRotate180(image_t *img)
{
    uint32_t aantal_pixels  = (img->width * img->height);

    // Calculate size, half of image 4 bytes per word =/8, with 4 loop unroll =/4 = 32
    register uint32_t words = (aantal_pixels / 8);

    // word pointers
    register uint32_t *img_data = (uint32_t*)img->data;
    register uint32_t *img_data_inv = img_data + (words * 2) - 1;
    register uint32_t t;

    // Check __REV availability (__REV ARMv7 insctruction to endian swap bytes)
#ifndef __arm__
#ifdef __QT__
#define __REV(x)    (qbswap(x))
#else
#define __REV(x) (0 | ((x & 0x000000ff) << 24) | ((x & 0x0000ff00) << 8) | ((x & 0x00ff0000) >> 8) | ((x & 0xff000000) >> 24))
#endif
#endif

    // Fast rotate 32 bit words
    while(words--){
        // unroll 1
        t = __REV(*img_data);
        *img_data++ = __REV(*img_data_inv);
        *img_data_inv-- = t;
    }
}

void vContrastStretchFast(image_t *src, // must be a greyscale image
                          image_t *dst) // result is a greyscale image
{
    // This function requires a 256 byte block of ram
    uint32_t aantal_pixels = src->width * src->height;
    uint32_t lpixel = 0xFF, hpixel = 0;
    uint8_t lut[0xFF+1];
    /* Step 1: Find high an low pixel values */
    {
        uint8_t *src_data = &src->data[0][0];
        uint32_t i = aantal_pixels, t;
        while(i--){
            t = *src_data++;
            if( t < lpixel ){
                lpixel =  t;
                continue;
            }
            if( t > hpixel ){
                hpixel = t;
                continue;
            }
        }
    }
    // Step 2: Prevent divide by zero
    {
        if(hpixel == lpixel){
            hpixel = 255;
            lpixel = 0;
        }
    }
    // Step 3: Calculate stretching table
    {
        uint8_t *plut = lut;
        uint32_t i = sizeof(lut);
        float stretch  = ((float)0xFF/(float)((float)hpixel-(float)lpixel));
        float rounding = 0.5;
        float t;
        uint32_t x=0;
        while(i--){
            t = x++ - lpixel;
            t *= stretch;
            t += rounding;
            *plut++ = t;
        }
    }
    // Step 4: Stretch image
    {
        // Pointers
        uint8_t* src_data = &src->data[0][0];   // Reset pointer
        uint8_t* dst_data = &dst->data[0][0];   // Reset pointer
        // Assign
        uint32_t i = aantal_pixels/4;
        while(i--){
            *dst_data++ = lut[*src_data++];
            *dst_data++ = lut[*src_data++];
            *dst_data++ = lut[*src_data++];
            *dst_data++ = lut[*src_data++];
        }
    }
}

// ----------------------------------------------------------------------------
void vErase(image_t *img)
{
    // Fill image with 0x00 or 0xFF
    const uint32_t aantal_pixels  = img->width * img->height;
    const uint32_t fill = 0x00000000;
    // Calculate size
    uint32_t words   = aantal_pixels / 4;

    // Create word aligned pointer, image is aligned
    uint32_t *img_data       = (uint32_t*)&img->data[0][0];
    while( words-- ){
        *img_data++ = fill;
    }
}

// ----------------------------------------------------------------------------
void vCopy(image_t *src,
           image_t *dst)
{
    // if(src != dst){
    const uint32_t aantal_pixels  = src->width * src->height;
    // Word Pointers
    uint32_t *src_data_w = (uint32_t *)&src->data[0][0];
    uint32_t *dst_data_w = (uint32_t *)&dst->data[0][0];

    // Counters
    uint32_t words   = aantal_pixels / 4;

    // Copy unaligned
    while( words-- )
    {
        *dst_data_w++ = *src_data_w++;
    }
    // }
}

// ----------------------------------------------------------------------------
void vAdd(image_t *src,
          image_t *dst)
{
    const uint32_t aantal_pixels  = src->width * src->height;
    // Byte Pointers
    uint8_t *src_data_b = &src->data[0][0];
    uint8_t *dst_data_b = &dst->data[0][0];

#if __arm__
    // Use SIMD unsigned quad saturated add
    // Word Pointers
    uint32_t *src_data_w;
    uint32_t *dst_data_w;
    // Word counters
    uint32_t words   = aantal_pixels / 4;
    // Byte to Word pointer
    src_data_w = (uint32_t*)src_data_b;
    dst_data_w = (uint32_t*)dst_data_b;

    // Copy unaligned
    while( words-- )
    {
        register uint32_t ans = __UQADD8(*src_data_w++, *dst_data_w);
        *dst_data_w++ = ans;
    }
#else
    // Separate additions
    uint32_t bytes = aantal_pixels;
    // Copy remains
    while ( bytes-- )
    {
        uint32_t t = 0;
        t = *dst_data_b + *src_data_b++;
        (t>0xFF)?(*dst_data_b++=0xFF):(*dst_data_b++=t);
    }
#endif
}

// ----------------------------------------------------------------------------
void vMultiply(image_t *src,
               image_t *dst)
{
    const uint32_t aantal_pixels  = src->width * src->height;
    // Byte Pointers
    uint8_t *src_data_b = &src->data[0][0];
    uint8_t *dst_data_b = &dst->data[0][0];

    // Separate additions
    uint32_t bytes = aantal_pixels;
    // Copy remains
    while ( bytes-- )
    {
        // Saturated multiply
        uint32_t t = 0;
        t = *dst_data_b * *src_data_b++;
        (t>0xFF)?(*dst_data_b++=0xFF):(*dst_data_b++=t);
    }
}

// ----------------------------------------------------------------------------
void vSetSelectedToValue(image_t *src,
                         image_t *dst,
                         uint8_t selected,
                         uint8_t value)
{
    const uint32_t aantal_pixels  = src->width * src->height;
    // Byte Pointers
    uint8_t *src_data_b = &src->data[0][0];
    uint8_t *dst_data_b = &dst->data[0][0];
    uint32_t bytes = aantal_pixels;
    // Run bytes
    while ( bytes-- )
    {
        uint32_t t = *src_data_b++;
        if(t == selected)
            *dst_data_b++ = value;
        else
            *dst_data_b++ = t;
    }
}

// ----------------------------------------------------------------------------
void vSetBorders(image_t *src,
                 image_t *dst,
                 uint8_t value)
{
    uint32_t mask;
    // Copy image
    vCopy(src,dst);

    // Masker
    mask = value | value<<8 | value<<16 | value<<24;
    // Set top+bottom
    {
        // Top word pointer
        uint32_t *dst_data_t = (uint32_t *)&dst->data[0][0];
        // Bottom word pointer
        uint32_t *dst_data_b = (uint32_t *)&dst->data[dst->height-1][0];
        // Loop
        uint32_t i = src->width/4;
        while ( i-- ){
            *dst_data_t++ = mask;
            *dst_data_b++ = mask;
        }
    }
    // Set sides
    {
        uint8_t *dst_data_l = &dst->data[1][0];
        uint8_t *dst_data_r = &dst->data[1][dst->width - 1];
        uint32_t i = dst->height-2;
        while ( i-- ){
            // Left
            *dst_data_l = value;
            dst_data_l += dst->width;
            // Right
            *dst_data_r = value;
            dst_data_r += dst->width;
        }
    }
}

// ----------------------------------------------------------------------------
void vBitSlicing(image_t *src,
                 image_t *dst,
                 uint8_t mask)
{
    const uint32_t aantal_pixels  = src->width * src->height;
    // Masker
    uint32_t wmask = mask | mask<<8 | mask<<16 | mask<<24;
    // Top word pointer
    uint32_t *src_data = (uint32_t *)&src->data[0][0];
    uint32_t *dst_data = (uint32_t *)&dst->data[0][0];
    // Loop
    uint32_t i = aantal_pixels/4;
    while ( i-- ){
        *dst_data++ = *src_data++ & wmask;
    }
}

// ----------------------------------------------------------------------------
void vHistogram(image_t  *src,
                uint16_t *hist,
                uint32_t *sum)
{
    // Erase histogram
    {
        uint32_t *data = (uint32_t *)hist;
        uint32_t i = 256/2;
        while(i--){
            *data++ = 0x0000000000;
        }
    }
    // Fill histogram
    {
        uint32_t i = src->height * src->width;
        uint8_t *data = &src->data[0][0];
        *sum = 0;
        while(i--){
            *sum += *data;
            hist[*data++]++;
        }
    }
}

// ----------------------------------------------------------------------------
void vThresholdIsoData(image_t *src,
                       image_t *dst,
                       eBrightness brightness) // DARK | BRIGHT
{
    // Get histogram
    uint16_t hist[256];
    uint32_t src_sum = 0;
    uint32_t lpixel = 256;
    uint32_t hpixel = 0;
    uint32_t T      = 127;

    vHistogram(src,hist,&src_sum);

    // Find lpixel & hpixel from histogram
    {
        uint32_t i = 256;
        while(i--){
            uint32_t a = 0;
            if( hist[i] ){
                a = i;
                if( a > hpixel )
                    hpixel = i;
                if( a < lpixel )
                    lpixel = i;
            }
        }
    }

    // Exceptions
    if(hpixel == lpixel){
        vThreshold(src,dst,lpixel,hpixel);
        QDEBUG("vThresholdIsoData: equal image");
        return;
    }else{
        T = lpixel;
        // 2 Means
        {
            uint32_t preT   = 0xFF;
            do{
                uint32_t meanLeft, meanRight;
                // Calculate meanLeft
                {
                    uint32_t i, sum = 0, px = 0;
                    for(i=0; i<=T; i++){
                        sum += i * hist[i];
                        px += hist[i];
                    }
                    if(px == 0){
                        QDEBUG("vThresholdIsoData: div/0 error");
                        return;
                    }
                    meanLeft = sum / px;
                }
                // Calculate meanRight
                {
                    uint32_t i, sum = 0, px = 0;
                    for(i=255; i>T; i--){
                        sum += i * hist[i];
                        px += hist[i];
                    }
                    if(px == 0){
                        QDEBUG("vThresholdIsoData: div/0 error");
                        return;
                    }
                    meanRight = sum / px;
                }
                // New T Mean MeanLeft+MeanRight
                preT = T;
                T = (meanLeft + meanRight)/2;
            }while(T != preT);
        }
        // Threshold
        {
            if(brightness == DARK){
                vThreshold(src,dst,0,T);
            }else{
                vThreshold(src,dst,T,0xFF);
            }
        }
    }
}

void vThresholdOtsu(image_t *src, image_t *dst, eBrightness brightness)
{
    // Get histogram
    uint16_t hist[256];
    uint32_t som = 0;
    uint32_t n = src->width*src->height;
    uint8_t T  = 127;

    vHistogram(src,hist,&som);
    {
        uint32_t j;
        float n_f    = 0;    // (foreground) Number of pixels
        float som_f  = 0;    // (foreground) Sum of pixel values
        float gem_f  = 0;    // (foreground) Pixel average
        float n_b    = 0;    // (background) Number of pixels
        float som_b  = 0;    // (background) Sum of pixel values
        float gem_b  = 0;    // (background) Pixel average
        float max_bcv = 0;
        float u ;
        float bcv;
        uint16_t max_bcv_i = 0;

        for(j=0; j<256; j++){
            // Left
            n_f += hist[j];
            som_f += j * hist[j];
            (n_f != 0)?(gem_f = som_f / n_f):(gem_f = 1);
            // Right
            n_b = n - n_f;
            som_b = som - som_f;
            (n_b != 0)?(gem_b = som_b / n_b):(gem_b = 1);
            // BetweenClass Variance (BCV)
            u 	= (gem_b - gem_f);
            bcv =  (n_b * n_f) * (u*u);
            if( bcv > max_bcv ){
                max_bcv = bcv;
                max_bcv_i = j;
            }
        }
        T = max_bcv_i;
        QDEBUG("T = " << T );
    }
    // Threshold
    {
        if(brightness == DARK){
            vThreshold(src,dst,0,T);
        }else{
            vThreshold(src,dst,T,0xFF);
        }
    }
}

// ----------------------------------------------------------------------------
void vFillHoles(image_t *src, // must be a binary image
                image_t *dst,
                eConnected connected) // FOUR | EIGHT
{
    // Step 0: vCOpy
    vCopy(src,dst);

    // Step 1:  Mark all background border pixels
    // Set top+bottom
    {
        // Top word pointer
        uint8_t *src_data_t = &src->data[0][0];
        uint8_t *dst_data_t = &dst->data[0][0];
        // Bottom word pointer
        uint8_t *src_data_b = &src->data[dst->height-1][0];
        uint8_t *dst_data_b = &dst->data[dst->height-1][0];
        // Loop
        uint8_t i = src->width;
        while ( i-- ){
            if(*src_data_t++ == 0) *dst_data_t++ = 2; else *dst_data_t++ = 1;
            if(*src_data_b++ == 0) *dst_data_b++ = 2; else *dst_data_b++ = 1;
        }
    }
    // Set sides
    {
        // Left pointer
        uint8_t *src_data_l = &src->data[1][0];
        uint8_t *dst_data_l = &dst->data[1][0];
        // Right pointer
        uint8_t *src_data_r = &src->data[1][dst->width - 1];
        uint8_t *dst_data_r = &dst->data[1][dst->width - 1];
        uint32_t i = dst->height-2;
        while ( i-- ){
            // Left
            if(*src_data_l == 0) *dst_data_l = 2; else *dst_data_l = 1;
            src_data_l += dst->width;
            dst_data_l += dst->width;
            // Right
            if(*src_data_r == 0) *dst_data_r = 2; else *dst_data_r = 1;
            src_data_r += dst->width;
            dst_data_r += dst->width;
        }
    }

    // Step 2:  Fill background area
    {
        uint32_t iterations = 0;
        uint32_t found  = 1;
        uint32_t width  = dst->width;
        uint32_t height = dst->height;
        while(found != 0){
            uint32_t x,y;
            found = 0;
            for(y=0; y < height; y++){
                for(x=0; x < width; x++){
                    if(src->data[y][x] == 0){
                        if(iNeighbourCount(src,x,y,2,connected)){
                            dst->data[y][x] = 2;
                            found++;
                        }
                    }
                }
            }
            if(found != 0){
                for(x=0; x < width; x++){
                    for(y=0; y < height; y++){
                        if(src->data[y][x] == 0){
                            if(iNeighbourCount(src,x,y,2,connected)){
                                dst->data[y][x] = 2;
                                found++;
                            }
                        }
                    }
                }
            }
            iterations++;
            QDEBUG("Filled holes in "<<iterations<<" iterations + border++");
        }
    }
    // Step 3:  Fill Holes
    vSetSelectedToValue(src,dst,0,1);
    // Step 4:  Remove selection
    vSetSelectedToValue(src,dst,2,0);

}

// ----------------------------------------------------------------------------
void vRemoveBorderBlobs(image_t *src, // must be a binary image
                        image_t *dst,
                        eConnected connected) // FOUR | EIGHT
{
    // Step 1:  Increment border blob pixels on image edge
#if 0   // Fix for bottom line constant value issue on evdk target
    // Set top+bottom
    {
        // Top word pointer
        uint8_t *src_data_t = &src->data[0][0];
        uint8_t *dst_data_t = &dst->data[0][0];
        // Bottom word pointer
        uint8_t *src_data_b = &src->data[dst->height-1][0];
        uint8_t *dst_data_b = &dst->data[dst->height-1][0];
        // Loop
        uint8_t i = src->width;
        while ( i-- ){
            if(*src_data_t++ == 1) *dst_data_t = 2; else *dst_data_t = 0;
            if(*src_data_b++ == 1) *dst_data_b = 2; else *dst_data_b = 0;
            dst_data_t++;
            dst_data_b++;
        }
    }
    // Set sides
    {
        uint8_t *src_data_l = &src->data[1][0];
        uint8_t *dst_data_l = &dst->data[1][0];
        uint8_t *src_data_r = &src->data[1][dst->width - 1];
        uint8_t *dst_data_r = &dst->data[1][dst->width - 1];
        uint32_t i = dst->height-2;
        while ( i-- ){
            // Left
            if(*src_data_l == 1) *dst_data_l = 2; else *dst_data_l = 0;
            src_data_l += dst->width;
            dst_data_l += dst->width;
            // Right
            if(*src_data_r == 1) *dst_data_r = 2; else *dst_data_r = 0;
            src_data_r += dst->width;
            dst_data_r += dst->width;
        }
    }
#else
    vSetBorders(src,dst,2);
#endif

    // Step 2:  Loop to set all border blob pixels to 2
    {
        uint32_t iterations = 0;
        uint32_t found = 1;
        uint32_t width = dst->width;
        uint32_t height = dst->height;
        while(found != 0){
            uint32_t x,y;
            found = 0;
            for(y=0; y < height; y++){
                for(x=0; x < width; x++){
                    if(dst->data[y][x] == 1){
                        if(iNeighbourCount(dst,x,y,2,connected)){
                            dst->data[y][x] = 2;
                            found++;
                        }
                    }
                }
            }
            if(found != 0){
                for(x=0; x < width; x++){
                    for(y=0; y < height; y++){
                        if(dst->data[y][x] == 1){
                            if(iNeighbourCount(dst,x,y,2,connected)){
                                dst->data[y][x] = 2;
                                found++;
                            }
                        }
                    }
                }
            }
            iterations++;
        }
        QDEBUG("Removed border blobs in "<<iterations<<" iterations + border++");
    }

    // Step 3:  Remove all selected borderblob pixels
    vSetSelectedToValue(src,dst,2,0);
}

// ----------------------------------------------------------------------------
void vFindEdges(image_t *src, // must be a binary image
                image_t *dst,
                eConnected connected)
{

    // Step 1:  Loop to mark all blob pixels next to background 2
    {
        uint32_t found = 1;
        uint32_t width = dst->width;
        uint32_t height = dst->height;
        uint32_t x	=0, y=0;
        uint8_t connected_filter = 4;
        if(connected == EIGHT){
            connected_filter = 8;
        }
        for(y=0; y < height; y++){
            for(x=0; x < width; x++){
                if(dst->data[y][x] == 1){
                    if(iNeighboursEqualOrHigher(src,x,y,1,connected) == connected_filter){
                        dst->data[y][x] = 2;
                        found++;
                    }
                    dst->data[y][x] = src->data[y][x];
                }else{
                    dst->data[y][x] = src->data[y][x];

                }
            }
        }
    }

    // Step 3:  Remove all selected borderblob pixels
    vSetSelectedToValue(src,dst,2,0);
}

// ----------------------------------------------------------------------------
uint32_t iLabelBlobs(image_t *src, // must be a binary image
                     image_t *dst,
                     eConnected connected)
{
    uint32_t iterations = 0;
    uint32_t width = dst->width;
    uint32_t height = dst->height;
    uint32_t npixels = width*height;
    uint32_t y,x;
    uint8_t blob_cnt = 0;
    uint32_t non_marked;
    uint8_t failed = 0;

    // Step 1: Mark all pixels
    vSetSelectedToValue(src,dst,1,255);

    // Step 2: Label blobs with blob_cnt
    // Loop until no changes
    iterations = 0;
    do{
        // Reset non_marked
        non_marked = 2*npixels;
        // Scan horizontal
        for(y=0;y<height; y++){
            for(x=0; x<width; x++){
                // Select marked pixels
                if(dst->data[y][x] == 255){
                    uint8_t nb = iNeighboursLowerNotZero(dst,x,y,255,connected);
                    if(nb){
                        // Blob pixel with neighouring labels, label with lowest neighbour label
                        dst->data[y][x] = nb;
                    }else{
                        // Blob pixel without label and neighbouring labels
                        dst->data[y][x] = ++blob_cnt;
                        // Check if labeling fails due to limited pixel wordlength
                        if(blob_cnt >= 254) return 0xFF;
                    }
                }else{
                    // Background pixel
                    non_marked--;
                }
            }
        }
        // Scan vertical
        for(x=0; x<width; x++){
            for(y=0;y<height; y++){
                // Select marked pixels
                if(dst->data[y][x] == 255){
                    uint8_t nb = iNeighboursLowerNotZero(dst,x,y,255,connected);
                    if(nb){
                        // Blob pixel with neighouring labels, label with lowest neighbour label
                        dst->data[y][x] = nb;
                    }else{
                        // Blob pixel without label and neighbouring labels
                        dst->data[y][x] = ++blob_cnt;
                        // Check if labeling fails due to limited pixel wordlength
                        if(blob_cnt >= 254) {
                            // Break
                            failed = 1;
                            x = width + 1;
                            y = height + 1;
                            non_marked = 0;
                        }
                    }
                }else{
                    // Background pixel
                    non_marked--;
                }
            }
        }
        // Infinite loop protect
        if(iterations++ > 100){
            failed = 1;
            break;
        }
    }while(non_marked);

    // Step 3: Loop and set any non-zero pixels to lowest non-zero neighbour
    // Loop until no changes
    if(!failed){
        do{
            non_marked = 2*npixels;
            for(y=0;y<height; y++){
                for(x=0; x<width; x++){
                    // Select non-background pixels
                    if(dst->data[y][x]){
                        uint8_t nb = iNeighboursLowerNotZero(dst,x,y,254,connected);
                        if(dst->data[y][x] != nb){
                            // Label pixel with neighouring lower labels
                            dst->data[y][x] = nb;
                        }else{
                            // Already labeled
                            non_marked--;
                        }
                    }else{
                        // Background
                        non_marked--;
                    }
                }
            }
            if(non_marked){
                for(x=0; x<width; x++){
                    for(y=0;y<height; y++){
                        // Select non-background pixels
                        if(dst->data[y][x]){
                            uint8_t nb = iNeighboursLowerNotZero(dst,x,y,254,connected);
                            if(dst->data[y][x] != nb){
                                // Label pixel with neighouring lower labels
                                dst->data[y][x] = nb;
                            }else{
                                // Already labeled
                                non_marked--;
                            }
                        }else{
                            // Background
                            non_marked--;
                        }
                    }
                }
            }
        }while(non_marked);
    }

    // Step 4: Correct label numbers
    if(!failed){
        uint8_t listLabels[255];    // Unused element 0
        uint32_t i 	= npixels;
        uint32_t label = 1;
        uint8_t *data = &dst->data[0][0];
        uint8_t pLabel = 0;
        blob_cnt = 0;
        // Create a list of used labels in array, automatically sorted due to method of labeling
        while(i--){
            if(*data > pLabel){
                listLabels[label++] = *data;
                pLabel = *data;
                blob_cnt++;
            }
            data++;
        }
        // replace pixels with array value to array index
        i = blob_cnt;
        label = 1;
        while(i--){
            vSetSelectedToValue(dst,dst,listLabels[label],label);
            label++;
        }
    }

    if(failed){
        vCopy(src,dst);
        return 0xFF;
    }

    return blob_cnt;
}

// ----------------------------------------------------------------------------
void vBlobAnalyse(image_t *img,
                  const uint8_t blobcount,
                  blobinfo_t *pBlobInfo)
{
    uint16_t end_y[254],end_x[254];
    uint32_t label;
    uint32_t w = img->width ,h = img->height, x,y;
		const double SQRT2 = sqrt(2.0); // 1.41421356237309514547462185874
		const double SQRT5 = sqrt(5.0); // 2.23606797749978980505147774238

    /* Erase memory */
    {
        uint32_t *pdatax = (uint32_t*)end_x, *pdatay=(uint32_t*)end_y, i = 254/2;
        while(i--){
            *pdatax++ = 0; *pdatay++ = 0;
        }
    }

    // Erase structs
    for(label=0; label<blobcount; label++){
        pBlobInfo[label].height        =   0;
        pBlobInfo[label].width         =   0xFFFF;
        pBlobInfo[label].nof_pixels    =   0;
        pBlobInfo[label].perimeter     =   0.0;
    }

    // Run image
    for(y=0;y<h;y++){
        for(x=0;x<w;x++){
            uint8_t label = img->data[y][x];
            // Check pixel boundaries
            if(label != 0){
                uint8_t edge = 0;
                label--;
                // Get width,height
                if(!pBlobInfo[label].height)
                    pBlobInfo[label].height = y;
                end_y[label] = y;
                if(x < pBlobInfo[label].width)
                    pBlobInfo[label].width = x;
                if(x > end_x[label])
                    end_x[label] = x;
                // Get nof
                pBlobInfo[label].nof_pixels++;
                // Get perimeter
                edge = iNeighbourCount(img,x,y,0,FOUR);
                if(edge == 1){
                    pBlobInfo[label].perimeter += 1;
                }else if(edge == 2){
                    pBlobInfo[label].perimeter += SQRT2; // SQRT(2)
                }else if(edge == 3){
                    pBlobInfo[label].perimeter += SQRT5; // SQRT(5)
                }
            }
        }
    }
    // Calculate width+heigth
    label = blobcount;
    for(label=0; label<blobcount; label++){
        // delta = end - start
        pBlobInfo[label].height        =   end_y[label] - pBlobInfo[label].height;
        pBlobInfo[label].width         =   end_x[label] - pBlobInfo[label].width;
    }
}

void vCentroid(image_t *img, uint8_t blobnr, uint8_t *xc, uint8_t *yc)
{
#warning EVD1 To be implemented: vCentroid

    // Prevent 'unused parameter' warnings
    // Remove this code when implementing starts
    img=img;
    blobnr=blobnr;
    xc=xc;
    yc=yc;
}

double dNormalizedCentralMoments(image_t *img, uint8_t blobnr, int p, int q)
{
#warning EVD1 To be implemented: dNormalizedCentralMoments

    // Prevent 'unused parameter' warnings
    // Remove this code when implementing starts
    img=img;
    blobnr=blobnr;
    p=p;
    q=q;

    // Prevent 'no return statement' warning
    // Remove this code when implementing starts
    return(0);
}

uint8_t iNeighbourCount(image_t *img,
                        uint16_t x,
                        uint16_t y,
                        uint8_t value,
                        eConnected connected)
{
    /* Border exceptions:
     *  1   Top + Left
     *  2   Top
     *  3   Top + Right
     *  4   Left
     *  5   Center
     *  6   Right
     *  7   Bottom Left
     *  8   Bottom
     *  9   Bottom Right
     *
     *  Pointer modifications
     *  -w-1    -w      -w+1
     *  -1      [p]     +1
     *  -w+1    +w      +w+1
     */

    // Helpers
    uint8_t *data = &img->data[y][x];
    uint32_t w = img->width;
    uint32_t h = img->height;
    uint32_t neighbours = 0;

    // Top exception
    if(y == 0){
        // Left exception
        if(x == 0){
            // Exception 1
            if(*(data + 1) == value) neighbours++;
            if(*(data + w) == value) neighbours++;
            if(connected == EIGHT){
                if(*(data + w + 1) == value) neighbours++;
            }
        }else if(x == w-1){
            // Exception 3
            if(*(data - 1 ) == value) neighbours++;
            if(*(data + w ) == value) neighbours++;
            if(connected == EIGHT){
                if(*(data + w + 1 ) == value) neighbours++;
            }
        }else{
            // Exception 2
            if(*(data + 1 ) == value) neighbours++;
            if(*(data - 1 ) == value) neighbours++;
            if(*(data + w ) == value) neighbours++;
            if(connected == EIGHT){
                if(*(data + w + 1 ) == value) neighbours++;
                if(*(data + w - 1 ) == value) neighbours++;
            }
        }
        // Bottom exception
    }else if(y == h-1){
        // Left exception
        if(x == 0){
            // Exception 7
            if(*(data + 1 ) == value) neighbours++;
            if(*(data - w ) == value) neighbours++;
            if(connected == EIGHT){
                if(*(data - w + 1 ) == value) neighbours++;
            }
        }else if(x == w-1){
            // Exception 9
            if(*(data - 1 ) == value) neighbours++;
            if(*(data - w ) == value) neighbours++;
            if(connected == EIGHT){
                if(*(data - w - 1 ) == value) neighbours++;
            }
        }else{
            // Exception 8
            if(*(data + 1 ) == value) neighbours++;
            if(*(data - 1 ) == value) neighbours++;
            if(*(data - w ) == value) neighbours++;
            if(connected == EIGHT){
                if(*(data - w + 1 ) == value) neighbours++;
                if(*(data - w - 1 ) == value) neighbours++;
            }
        }
    }else{
        // Left exception
        if(x == 0){
            // Exception 4
            if(*(data + 1 ) == value) neighbours++;
            if(*(data + w ) == value) neighbours++;
            if(*(data - w ) == value) neighbours++;
            if(connected == EIGHT){
                if(*(data + w + 1 ) == value) neighbours++;
                if(*(data - w + 1 ) == value) neighbours++;
            }
        }else if(x == w-1){
            // Exception 6
            if(*(data - 1 ) == value) neighbours++;
            if(*(data + w ) == value) neighbours++;
            if(*(data - w ) == value) neighbours++;
            if(connected == EIGHT){
                if(*(data + w + 1 ) == value) neighbours++;
                if(*(data - w - 1 ) == value) neighbours++;
            }
        }else{
            // Exception 5
            if(*(data + 1 ) == value) neighbours++;
            if(*(data - 1 ) == value) neighbours++;
            if(*(data + w ) == value) neighbours++;
            if(*(data - w ) == value) neighbours++;
            if(connected == EIGHT){
                if(*(data + w + 1 ) == value) neighbours++;
                if(*(data + w - 1 ) == value) neighbours++;
                if(*(data - w + 1 ) == value) neighbours++;
                if(*(data - w - 1 ) == value) neighbours++;
            }
        }
    }
    return neighbours;
}

uint8_t iNeighboursEqualOrHigher(image_t *img,
                                 uint16_t x,
                                 uint16_t y,
                                 uint8_t value,
                                 eConnected connected)
{
    /* Border exceptions:
     *  1   Top + Left
     *  2   Top
     *  3   Top + Right
     *  4   Left
     *  5   Center
     *  6   Right
     *  7   Bottom Left
     *  8   Bottom
     *  9   Bottom Right
     *
     *  Pointer modifications
     *  -w-1    -w      -w+1
     *  -1      [p]     +1
     *  -w+1    +w      +w+1
     */

    // Helpers
    uint8_t *data = &img->data[y][x];
    uint32_t w = img->width;
    uint32_t h = img->height-1;
    uint32_t neighbours = 0;

    // Top exception
    if(y == 0){
        // Left exception
        if(x == 0){
            // Exception 1
            if(*(data + 1) >= value) neighbours++;
            if(*(data + w) >= value) neighbours++;
            if(connected == EIGHT){
                if(*(data + w + 1) >= value) neighbours++;
            }
        }else if(x == w-1){
            // Exception 3
            if(*(data - 1 ) >= value) neighbours++;
            if(*(data + w ) >= value) neighbours++;
            if(connected == EIGHT){
                if(*(data + w + 1 ) >= value) neighbours++;
            }
        }else{
            // Exception 2
            if(*(data + 1 ) >= value) neighbours++;
            if(*(data - 1 ) >= value) neighbours++;
            if(*(data + w ) >= value) neighbours++;
            if(connected == EIGHT){
                if(*(data + w + 1 ) >= value) neighbours++;
                if(*(data + w - 1 ) >= value) neighbours++;
            }
        }
        // Bottom exception
    }else if(y == h){
        // Left exception
        if(x == 0){
            // Exception 7
            if(*(data + 1 ) >= value) neighbours++;
            if(*(data - w ) >= value) neighbours++;
            if(connected == EIGHT){
                if(*(data - w + 1 ) >= value) neighbours++;
            }
        }else if(x == w-1){
            // Exception 9
            if(*(data - 1 ) >= value) neighbours++;
            if(*(data - w ) >= value) neighbours++;
            if(connected == EIGHT){
                if(*(data - w - 1 ) >= value) neighbours++;
            }
        }else{
            // Exception 8
            if(*(data + 1 ) >= value) neighbours++;
            if(*(data - 1 ) >= value) neighbours++;
            if(*(data - w ) >= value) neighbours++;
            if(connected == EIGHT){
                if(*(data - w + 1 ) >= value) neighbours++;
                if(*(data - w - 1 ) >= value) neighbours++;
            }
        }
    }else{
        // Left exception
        if(x == 0){
            // Exception 4
            if(*(data + 1 ) >= value) neighbours++;
            if(*(data + w ) >= value) neighbours++;
            if(*(data - w ) >= value) neighbours++;
            if(connected == EIGHT){
                if(*(data + w + 1 ) >= value) neighbours++;
                if(*(data - w + 1 ) >= value) neighbours++;
            }
        }else if(x == w-1){
            // Exception 6
            if(*(data - 1 ) >= value) neighbours++;
            if(*(data + w ) >= value) neighbours++;
            if(*(data - w ) >= value) neighbours++;
            if(connected == EIGHT){
                if(*(data + w + 1 ) >= value) neighbours++;
                if(*(data - w - 1 ) >= value) neighbours++;
            }
        }else{
            // Exception 5
            if(*(data + 1 ) >= value) neighbours++;
            if(*(data - 1 ) >= value) neighbours++;
            if(*(data + w ) >= value) neighbours++;
            if(*(data - w ) >= value) neighbours++;
            if(connected == EIGHT){
                if(*(data + w + 1 ) >= value) neighbours++;
                if(*(data + w - 1 ) >= value) neighbours++;
                if(*(data - w + 1 ) >= value) neighbours++;
                if(*(data - w - 1 ) >= value) neighbours++;
            }
        }
    }
    return neighbours;
}

// Returns the lowest argument
uint8_t lowest(uint8_t a1, uint8_t a2, uint8_t a3, uint8_t a4, uint8_t a5, uint8_t a6, uint8_t a7, uint8_t a8)
{
    uint8_t sLow = 0xFF;
    if( a1 != 0 ) if(a1 < sLow) sLow = a1;
    if( a2 != 0 ) if(a2 < sLow) sLow = a2;
    if( a3 != 0 ) if(a3 < sLow) sLow = a3;
    if( a4 != 0 ) if(a4 < sLow) sLow = a4;
    if( a5 != 0 ) if(a5 < sLow) sLow = a5;
    if( a6 != 0 ) if(a6 < sLow) sLow = a6;
    if( a7 != 0 ) if(a7 < sLow) sLow = a7;
    if( a8 != 0 ) if(a8 < sLow) sLow = a8;
    return sLow;
}

uint8_t iNeighboursLowerNotZero(image_t *img,
                                uint16_t x,
                                uint16_t y,
                                uint8_t value,
                                eConnected connected)
{
    /* Border exceptions:
     *  1   Top + Left
     *  2   Top
     *  3   Top + Right
     *  4   Left
     *  5   Center
     *  6   Right
     *  7   Bottom Left
     *  8   Bottom
     *  9   Bottom Right
     *
     *  Pointer modifications
     *  -w-1    -w      -w+1
     *  -1      [p]     +1
     *  -w+1    +w      +w+1
     */

    // Helpers
    uint8_t *data = &img->data[y][x];
    uint32_t w = img->width;
    uint32_t h = img->height-1;
    uint32_t neighbours = 0;

    /* Data modifiers */
    uint8_t *data_ul	= (data - w - 1 );	// Up Left
    uint8_t *data_u		= (data - w )	 ;  // Up
    uint8_t *data_ur	= (data - w + 1 );	// Up Right
    uint8_t *data_cl	= (data - 1 )	 ;  // Center Left
    uint8_t *data_cr	= (data + 1 )	 ;  // Center Right
    uint8_t *data_bl	= (data + w - 1 );	// Bottom Left
    uint8_t *data_b		= (data + w )	 ;  // Bottom
    uint8_t *data_br	= (data + w + 1 );	// Bottom Right

    // Top exception
    if(y == 0){
        // Left exception
        if(x == 0){
            // Exception 1
            if(*data_cr != 0 && *data_cr < value) neighbours++;
            if(*data_b != 0 && *data_b < value) neighbours++;
            if(connected == EIGHT){
                if(*data_br != 0 && *data_br < value) neighbours++;
                if(neighbours)	neighbours = lowest(*data_cr,*data_b,*data_br,0,0,0,0,0);
            }
            if(neighbours)	neighbours = lowest(*data_cr,*data_b,0,0,0,0,0,0);
        }else if(x == w-1){
            // Exception 3
            if(*data_cl != 0 && *data_cl < value) neighbours++;
            if(*data_b != 0 && *data_b < value) neighbours++;
            if(connected == EIGHT){
                if(*data_br != 0 && *data_br < value) neighbours++;
                if(neighbours)	neighbours = lowest(*data_cl,*data_b,*data_br,0,0,0,0,0);
            }
            if(neighbours)	neighbours = lowest(*data_cl,*data_b,0,0,0,0,0,0);
        }else{
            // Exception 2
            if(*data_cr != 0 && *data_cr < value) neighbours++;
            if(*data_cl != 0 && *data_cl < value) neighbours++;
            if(*data_b != 0 && *data_b < value) neighbours++;
            if(connected == EIGHT){
                if(*data_br != 0 && *data_br < value) neighbours++;
                if(*data_bl != 0 && *data_bl < value) neighbours++;
                if(neighbours)	neighbours = lowest(*data_cr,*data_cl,*data_b,*data_br,*data_bl,0,0,0);
            }
            if(neighbours)	neighbours = lowest(*data_cr,*data_cl,*data_b,0,0,0,0,0);
        }
        // Bottom exception
    }else if(y == h){
        // Left exception
        if(x == 0){
            // Exception 7
            if(*data_cr != 0 && *data_cr < value) neighbours++;
            if(*data_u != 0 && *data_u < value) neighbours++;
            if(connected == EIGHT){
                if(*data_ur != 0 && *data_ur < value) neighbours++;
                if(neighbours)	neighbours = lowest(*data_cr,*data_u,*data_ur,0,0,0,0,0);
            }
            if(neighbours)	neighbours = lowest(*data_cr,*data_u,0,0,0,0,0,0);
        }else if(x == w-1){
            // Exception 9
            if(*data_cl != 0 && *data_cl < value) neighbours++;
            if(*data_u != 0 && *data_u < value) neighbours++;
            if(connected == EIGHT){
                if(*data_ul != 0 && *data_ul < value) neighbours++;
                if(neighbours)	neighbours = lowest(*data_cl,*data_u,*data_ul,0,0,0,0,0);
            }
            if(neighbours)	neighbours = lowest(*data_cl,*data_u,0,0,0,0,0,0);
        }else{
            // Exception 8
            if(*data_cr != 0 && *data_cr < value) neighbours++;
            if(*data_cl != 0 && *data_cl < value) neighbours++;
            if(*data_u != 0 && *data_u < value) neighbours++;
            if(connected == EIGHT){
                if(*data_ur != 0 && *data_ur < value) neighbours++;
                if(*data_ul != 0 && *data_ul < value) neighbours++;
                if(neighbours)	neighbours = lowest(*data_cr,*data_cl,*data_u,*data_ur,*data_ul,0,0,0);
            }
            if(neighbours)	neighbours = lowest(*data_cr,*data_cl,*data_u,0,0,0,0,0);
        }
    }else{
        // Left exception
        if(x == 0){
            // Exception 4
            if(*data_cr != 0 && *data_cr < value) neighbours++;
            if(*data_b != 0 && *data_b < value) neighbours++;
            if(*data_u != 0 && *data_u < value) neighbours++;
            if(connected == EIGHT){
                if(*data_br != 0 && *data_br < value) neighbours++;
                if(*data_ur != 0 && *data_ur < value) neighbours++;
                if(neighbours)	neighbours = lowest(*data_cr,*data_b,*data_u,*data_br,*data_ur,0,0,0);
            }
            if(neighbours)	neighbours = lowest(*data_cr,*data_b,*data_u,0,0,0,0,0);
        }else if(x == w-1){
            // Exception 6
            if(*data_cl != 0 && *data_cl < value) neighbours++;
            if(*data_b != 0 && *data_b < value) neighbours++;
            if(*data_u != 0 && *data_u < value) neighbours++;
            if(connected == EIGHT){
                if(*data_br != 0 && *data_br < value) neighbours++;
                if(*data_ul != 0 && *data_ul < value) neighbours++;
                if(neighbours)	neighbours = lowest(*data_cl,*data_b,*data_u,*data_br,*data_ul,0,0,0);
            }
            if(neighbours)	neighbours = lowest(*data_cl,*data_b,*data_u,0,0,0,0,0);
        }else{
            // Exception 5
            if(*data_cr != 0 && *data_cr < value) neighbours++;
            if(*data_cl != 0 && *data_cl < value) neighbours++;
            if(*data_b != 0 && *data_b < value) neighbours++;
            if(*data_u != 0 && *data_u < value) neighbours++;
            if(connected == EIGHT){
                if(*data_br != 0 && *data_br < value) neighbours++;
                if(*data_bl != 0 && *data_bl < value) neighbours++;
                if(*data_ur != 0 && *data_ur < value) neighbours++;
                if(*data_ul != 0 && *data_ul < value) neighbours++;
                if(neighbours)	neighbours = lowest(*data_cr,*data_cl,*data_b,*data_u,*data_br,*data_bl,*data_ur,*data_ul);
            }
            if(neighbours)	neighbours = lowest(*data_cr,*data_cl,*data_b,*data_u,0,0,0,0);
        }
    }
    return neighbours;
}

image_roi_t sRoiValidate(image_roi_t roi){
#warning EVD1 To be implemented: sRoiValidate
/* Not required for imageToBCD, no word aligned access required */
    return roi;
}

/**
 *  @brief converts image of 7 segment to BCD
 *  @param[in] img  input image_t
 *  @return bcd
 */
int imageToBCD(image_t *img, image_roi_t roi){
    const uint8_t digit_list[16][7] = {
        /* A ,  B ,  C ,  D ,  E ,  F ,  G */
        {0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0x00},   // 0 - ABCDEF
        {0x00,0xFF,0xFF,0x00,0x00,0x00,0x00},   // 1 - BC
        {0xFF,0xFF,0x00,0xFF,0xFF,0x00,0xFF},   // 2 - ABDEG
        {0xFF,0xFF,0xFF,0xFF,0x00,0x00,0xFF},   // 3 - ABCDF
        {0x00,0xFF,0xFF,0x00,0x00,0xFF,0xFF},   // 4 - BCFG
        {0xFF,0x00,0xFF,0xFF,0x00,0xFF,0xFF},   // 5 - ACDFG
        {0xFF,0x00,0xFF,0xFF,0xFF,0xFF,0xFF},   // 6 - ACDEFG
        {0xFF,0xFF,0xFF,0x00,0x00,0xFF,0x00},   // 7 - ABCF
        {0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF},   // 8 - ABCDEFG
        {0xFF,0xFF,0xFF,0xFF,0x00,0xFF,0xFF},   // 9 - ABCDFG
        // Extra
        {0xFF,0xFF,0xFF,0x00,0xFF,0xFF,0xFF},   // A - ABCEFG
        {0x00,0x00,0xFF,0xFF,0xFF,0xFF,0xFF},   // B - CDEFG
        {0xFF,0x00,0x00,0xFF,0xFF,0xFF,0x00},   // C - ADEF
        {0x00,0xFF,0xFF,0xFF,0xFF,0x00,0xFF},   // D - BCDEG
        {0xFF,0x00,0x00,0xFF,0xFF,0xFF,0xFF},   // E - ADEFG
        {0xFF,0x00,0x00,0x00,0xFF,0xFF,0xFF}    // F - AEFG
    };
    uint8_t segments[7] = {0,0,0,0,0,0,0};
#define SEGMENT_A   0
#define SEGMENT_B   1
#define SEGMENT_C   2
#define SEGMENT_D   3
#define SEGMENT_E   4
#define SEGMENT_F   5
#define SEGMENT_G   6
    static uint32_t x;
    static uint32_t y;
    float digit_width ;
    float digit_heigth;
    float aspect_ratio;
    int result = -1;
#ifndef __arm__
    const char segmentchars[7] = {'A','B','C','D','E','F','G'};
#endif

    /* Step 1: Find beginning and end of digit */
    uint16_t start_x = 0xFFFF, end_x = 0;
    uint16_t start_y = 0xFFFF, end_y = 0;
    uint16_t w = roi.w + roi.x;
    uint16_t h = roi.h + roi.y;
    for(y = roi.y; y<h; y++){
        for(x = roi.x; x<w; x++){
            if(img->data[y][x]){
                // Relative x and y
                uint16_t rx = x - roi.x;
                uint16_t ry = y - roi.y;
                if(rx < start_x)
                    start_x = rx;
                if(rx > end_x)
                    end_x = rx;
                if(ry < start_y)
                    start_y = ry;
                if(ry > end_y)
                    end_y = ry;
            }
        }
    }
    // Offset fix
    end_x++; end_y++;


    // Step 2:
    // We know the ratios of 7 segment displays
    // Check aspect ratio of 0.5 for everything except the 1
    digit_width  = end_x - start_x;
    digit_heigth = end_y - start_y;
    aspect_ratio = digit_width / digit_heigth;
    if( aspect_ratio < 0.5f ){
        // This might be a 1
        return 1;
    }
    if( aspect_ratio > 0.7f ){
        return -1; // Invalid character
    }
    // Otherwise check for size
    if( digit_heigth < 10.0f){
        // Must be at least 10 pixel high
        return -2;
    }
    if( digit_width < 10.0f){
        // Must be at least 10 pixel wide
        return -3;
    }

    // Step 3: Scan for each segment
    // Scan for vertical segments at 1/3 and 2/3 of digit heigth
    {
        uint16_t img_half_x         = roi.x + (start_x + (digit_width / 2));
        uint16_t img_half_y_top     = roi.y + (start_y + (digit_heigth / 3));
        uint16_t img_half_y_bottom  = roi.y + (end_y - (digit_heigth / 3));
        uint16_t img_start_x        = roi.x + start_x;
        uint16_t img_end_x          = roi.x + start_x + (end_x - start_x);
        uint16_t img_start_y        = roi.y + start_y;
        uint16_t img_end_y          = roi.y + start_y + (end_y - start_y);
        // Segments F and E (left)
        for(x = img_start_x; x < img_half_x; x++){
            if(img->data[img_half_y_top][x]){
                segments[SEGMENT_F]++;
            }
            if(img->data[img_half_y_bottom][x]){
                segments[SEGMENT_E]++;
            }
        }
        // Segments B and C (right)
        for(x = img_half_x; x < img_end_x; x++){
            if(img->data[img_half_y_top][x]){
                segments[SEGMENT_B]++;
            }
            if(img->data[img_half_y_bottom][x]){
                segments[SEGMENT_C]++;
            }
        }
        // Scan for horizontal segments at 1/2 of start_x and end_x
        // Segment A
        for(y = img_start_y; y<img_half_y_top; y++){
            if(img->data[y][img_half_x]){
                segments[SEGMENT_A]++;
            }
        }
        // Segments G
        for(y = img_half_y_top; y<img_half_y_bottom; y++){
            if(img->data[y][img_half_x]){
                segments[SEGMENT_G]++;
            }
        }
        // Segments D
        for(y = img_half_y_bottom; y<img_end_y; y++){
            if(img->data[y][img_half_x]){
                segments[SEGMENT_D]++;
            }
        }
    }

    // Step 4: Lookup set segments to a number
    {
        uint8_t compare;
        for(y=0; y<16; y++){    // known digit_list
            compare = 0;
            for(x=0; x<7; x++){ // found set elements
                if(digit_list[y][x]){
                    if(segments[x]){
                        compare++;
                    }else{
                        compare = 0; break;
                    }
                }else{
                    if(segments[x]){
                        compare = 0; break;
                    }else{
                        compare++;
                    }
                }
            }
            if(compare){
                result = y;
                break;
            }
        }
        if(y >= 16) // No char in database
            result = -4;
    }

    //QDEBUG("SubRoi from x: " << start_x << " to " << end_x);
    //QDEBUG("SubRoi from y: " << start_y << " to " << end_y);
    //QDEBUG("SubRoi aspect ratio: " << aspect_ratio );

    for(x=0; x<7; x++)
        QDEBUG("Set segments: " << segmentchars[x] << " = " << segments[x]);

    QDEBUG("Found " << result);
    return result;
}

// ----------------------------------------------------------------------------
// EOF
// ----------------------------------------------------------------------------
