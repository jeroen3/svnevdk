#******************************************************************************
 # Project    : Embedded Vision Design
 # Copyright  : 2012 HAN Embedded Systems Engineering
 # Author     : Hugo
 #
 # Description: qmake project file
 #
 #*****************************************************************************
 # Change History:
 #
 #  Version 2.0 - October 2012
 #  > New folder structure
 #
 #  Version 1.0 - September 2011
 #  > Initial revision
 #  > Project created by QtCreator 2011-10-16T10:41:40
 #
#*****************************************************************************/
QT      +=  core gui widgets

# Enable this configuration for a debug console
CONFIG  += console

# Enbale Qt debugging
DEFINES += QDEBUG_ENABLE

# Set target and tamplate
TARGET   =  evdk
TEMPLATE =  app

# Set the include directories
INCLUDEPATH += ./ft245app         \
               ./main             \
               ./user

# Set the source files
SOURCES +=  main/main.cpp         \
            main/mainwindow.cpp   \
            user/operators.cpp    \
            user/VisionSet1.cpp   \
            user/VisionSet2.cpp   \
            user/VisionSet3.cpp   \
            user/VisionSet4.cpp   \
            user/VisionSet5.cpp   \
            user/VisionSet6.cpp   \
            user/VisionSet7.cpp   \
            user/VisionSet8.cpp   \
            user/VisionSet9.cpp   \
            ft245app/ft245app.cpp \
            ft245app/d2xx.cpp \
    user/extra_operators.cpp

# Set the header files
HEADERS +=  main/mainwindow.h     \
            user/operators.h      \
            user/VisionSet.h      \
            ft245app/ft245app.h   \
            ft245app/ftd2xx.h     \
            ft245app/d2xx.h \
    user/extra_operators.h

# Set the forms
FORMS   +=  main/mainwindow.ui    \
            ft245app/ft245app.ui

# Set the libraries that need to be copied and the destination path
ftd2xx_libs.files += ../evdk/WIN32/ftd2xx.dll \
                     ../evdk/WIN32/ftd2xx.lib
ftd2xx_libs.path   = $$OUT_PWD/debug

# Install additional files, 'make install' needs to be added as a custom build step
INSTALLS += ftd2xx_libs

# Additional libraries, windows only
win32: LIBS += ../evdk/WIN32/ftd2xx.lib

#project defines
DEFINES += __QT__
#QMAKE_CXXFLAGS += -Ofast

OTHER_FILES +=
