#include "VisionSet.h"
#include "led.h"

void VisionSet3(image_t *pSrc, image_t *pDst)
{
  benchmark_t bench;

  // 1. Original source image to PC
  pc_send_string("1. Source image");
  pSrc->lut = LUT_CLIP;
  pc_send_image(pSrc);

  // 2. Start a benchmark, execute vision operation and send info to PC
  benchmark_start(&bench, "vCopy");
  vCopy(pSrc, pDst);
  benchmark_stop(&bench);
  pc_send_benchmark(&bench);
  pDst->lut = LUT_STRETCH;
  pc_send_image(pDst);
  pc_send_string("2. vCopy");
  
  // Notice that from now on the destination image also is the source image!!

	// 3. Start a benchmark, execute vision operation and send info to PC
  benchmark_start(&bench, "Add");
  vAdd(pSrc, pDst);
  benchmark_stop(&bench);
  pc_send_benchmark(&bench);
  pDst->lut = LUT_STRETCH;
  pc_send_image(pDst);
  pc_send_string("3. vAdd");	
	
	// 4
	benchmark_start(&bench, "vThresholdIsoData");
  vThresholdIsoData(pSrc, pDst, BRIGHT);
  benchmark_stop(&bench);
  pc_send_benchmark(&bench);
  pDst->lut = LUT_BINARY;
  pc_send_image(pDst);
  pc_send_string("4. vThresholdIsoData");
	
	// 5
	benchmark_start(&bench, "vMultiply");
  vMultiply(pSrc, pDst);
  benchmark_stop(&bench);
  pc_send_benchmark(&bench);
  pDst->lut = LUT_CLIP;
  pc_send_image(pDst);
  pc_send_string("5. vMultiply");
	
	// 6
	benchmark_start(&bench, "vErase");
  vErase(pDst);
  benchmark_stop(&bench);
  pc_send_benchmark(&bench);
  pDst->lut = LUT_CLIP;
  pc_send_image(pDst);
  pc_send_string("6. vErase");
	
	// 7
	benchmark_start(&bench, "vSetBorders");
  vSetBorders(pDst, pDst, 127);
  benchmark_stop(&bench);
  pc_send_benchmark(&bench);
  pDst->lut = LUT_STRETCH;
  pc_send_image(pDst);
  pc_send_string("7. vSetBorders");
	
	// 8
	benchmark_start(&bench, "vBitSlicing");
  vBitSlicing(pDst, pDst, 127);
  benchmark_stop(&bench);
  pc_send_benchmark(&bench);
  pDst->lut = LUT_STRETCH;
  pc_send_image(pDst);
  pc_send_string("8. vBitSlicing");
	

  led_set_bus(1);
}
