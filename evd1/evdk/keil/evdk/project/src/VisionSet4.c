#include "VisionSet.h"

void VisionSet4(image_t *pSrc, image_t *pDst)
{
  benchmark_t bench;

  // 1. Original source image to PC
	benchmark_start(&bench, "Reference");
	benchmark_stop(&bench);
  pc_send_string("1. Source image");
  pSrc->lut = LUT_CLIP;
  pc_send_image(pSrc);

  // 2. Start a benchmark, execute vision operation and send info to PC
  benchmark_start(&bench, "ContrastStrtch");
  vContrastStretch(pSrc, pDst, 125, 150);
  benchmark_stop(&bench);
  pc_send_benchmark(&bench);
  pDst->lut = LUT_STRETCH;
  pc_send_image(pDst);
  pc_send_string("2. ContrastStretch");
	
	// 3. Start a benchmark, execute vision operation and send info to PC
  benchmark_start(&bench, "ContrastStrtchFast");
  vContrastStretchFast(pSrc, pDst);
  benchmark_stop(&bench);
  pc_send_benchmark(&bench);
  pDst->lut = LUT_STRETCH;
  pc_send_image(pDst);
  pc_send_string("3. ContrastStretchFast");

	// 4 Rotate
	benchmark_start(&bench, "Rotate180");
  vRotate180(pSrc);
  benchmark_stop(&bench);
  pc_send_benchmark(&bench);
  pc_send_image(pSrc);
  pc_send_string("4. Rotate180");
	
	// 5 Threshold
	__disable_irq();
	benchmark_start(&bench, "Threshold");
  vThreshold(pSrc,pDst,200,255);
  benchmark_stop(&bench);
		__enable_irq();
  pc_send_benchmark(&bench);
  pc_send_image(pDst);
  pc_send_string("5. Thrshld");
	
	// 5 Threshold ISO
	benchmark_start(&bench, "ThresholdISO");
  vThresholdIsoData(pSrc,pDst,BRIGHT);
  benchmark_stop(&bench);
  pc_send_benchmark(&bench);
  pc_send_image(pDst);
  pc_send_string("6. ThrshldISO");
  	
  // Notice that from now on the destination image also is the source image!!
  
  // 3. Start a benchmark, execute vision operation and send info to PC
  /*benchmark_start(&bench, "Threshold");
  vThreshold(pDst, pDst, 0, 100);
  benchmark_stop(&bench);
  pc_send_benchmark(&bench);
  pDst->lut = LUT_BINARY;
  pc_send_image(pDst);
  pc_send_string("3. Threshold 0-100");*/
	
}
