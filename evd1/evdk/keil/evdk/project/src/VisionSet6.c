#include "VisionSet.h"

void VisionSet6(image_t *pSrc, image_t *pDst)
{
  benchmark_t bench;

  // 1. Original source image to PC
  pc_send_string("1. Source image");
  pSrc->lut = LUT_CLIP;
  pc_send_image(pSrc);

	vErase(pDst);
	
  // 2. Start a benchmark, execute vision operation and send info to PC
  benchmark_start(&bench, "2. IncFillFor");
	{
		uint32_t x,y;
		uint32_t teller=0;
		for(y=0; y<pDst->height; y++){
				for(x=0; x<pDst->width; x++){
						pDst->data[y][x] = teller++;
				}
		}
	}
  benchmark_stop(&bench);
  pc_send_benchmark(&bench);
  pDst->lut = LUT_CLIP;
  pc_send_image(pDst);
  pc_send_string("2. IncFillFor");
  
  // Notice that from now on the destination image also is the source image!!
  vErase(pDst);
  // 3. Start a benchmark, execute vision operation and send info to PC
  benchmark_start(&bench, "3. IncFillWhile");
	{
		uint32_t teller=0;
		uint32_t i = pDst->height*pDst->width;
		uint8_t *data = &pDst->data[0][0];
		while(i--){
			*data++ = teller++;
		}
	}
  benchmark_stop(&bench);
  pc_send_benchmark(&bench);
  pDst->lut = LUT_BINARY;
  pc_send_image(pDst);
  pc_send_string("3. IncFillWhile");
}
