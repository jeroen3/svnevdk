#include "VisionSet.h"
#include "led.h"

/* Digit Detect */

void VisionSet2(image_t *pSrc, image_t *pDst)
{

  benchmark_t bench;
	image_roi_t pDstRoi;
	int bcd;
	
  // 2. PreProcessing
  benchmark_start(&bench, "vContrastStretchFast");
		vContrastStretchFast(pSrc,pDst);
	benchmark_stop(&bench);
	pc_send_benchmark(&bench);
	benchmark_start(&bench, "vThresholdIsoData");
		vThresholdIsoData(pDst,pDst,DARK);
	benchmark_stop(&bench);
	pc_send_benchmark(&bench);
	benchmark_start(&bench, "vRemoveBorderBlobs");
		vRemoveBorderBlobs(pDst,pDst,EIGHT);
	benchmark_stop(&bench);
	pc_send_benchmark(&bench);
  pDst->lut = LUT_BINARY;
  pc_send_image(pDst);
  pc_send_string("2. vPreProces");
	
	benchmark_start(&bench, "imageToBCD");
	pDstRoi.h = pSrc->height;
	pDstRoi.w = pSrc->width;
	pDstRoi.x = 0;
	pDstRoi.y = 0;
	
	pDstRoi = sRoiValidate(pDstRoi);
	bcd = imageToBCD(pDst,pDstRoi);	
	
	vRectangleRoi(pDst,pDstRoi,2);
	
	

  pc_send_benchmark(&bench);
  pc_send_image(pDst);
  //pc_send_string("3. imageToBCD");
	switch(bcd){
		case 0:
			pc_send_string("bcd: 0");
			led_set_bus(0x0F);	
		break;
		case 1:
			pc_send_string("bcd: 1");
			led_set_bus(bcd & 0x0F);	
		break;
		case 2:
			pc_send_string("bcd: 2");
			led_set_bus(bcd & 0x0F);	
		break;
		case 3:
			pc_send_string("bcd: 3");
			led_set_bus(bcd & 0x0F);	
		break;
		case 4:
			pc_send_string("bcd: 4");
			led_set_bus(bcd & 0x0F);	
		break;
		case 5:
			pc_send_string("bcd: 5");
			led_set_bus(bcd & 0x0F);	
		break;
		case 6:
			pc_send_string("bcd: 6");
			led_set_bus(bcd & 0x0F);	
		break;
		case 7:
			pc_send_string("bcd: 7");
			led_set_bus(bcd & 0x0F);	
		break;
		case 8:
			pc_send_string("bcd: 8");
			led_set_bus(bcd & 0x0F);	
		break;
		case 9:
			pc_send_string("bcd: 9");
			led_set_bus(bcd & 0x0F);	
		break;
		case 0xA:
			pc_send_string("bcd: A");
			led_set_bus(bcd & 0x0F);	
		break;
		case 0xB:
			pc_send_string("bcd: B");
			led_set_bus(bcd & 0x0F);	
		break;
		case 0xC:
			pc_send_string("bcd: C");
			led_set_bus(bcd & 0x0F);	
		break;
		case 0xD:
			pc_send_string("bcd: D");
			led_set_bus(bcd & 0x0F);	
		break;
		case 0xE:
			pc_send_string("bcd: E");
			led_set_bus(bcd & 0x0F);	
		break;
		case 0xF:
			pc_send_string("bcd: F");
			led_set_bus(bcd & 0x0F);	
		break;
		default:
			pc_send_string("bcd: ?");
			led_set_bus(0);	
		break;
	}	
}
