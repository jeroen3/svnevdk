#include "VisionSet.h"

void VisionSet5(image_t *pSrc, image_t *pDst)
{
  benchmark_t bench;

  // 1. Original source image to PC
  pc_send_string("1. Source image");
  pSrc->lut = LUT_CLIP;
  pc_send_image(pSrc);

	vContrastStretchFast(pSrc, pDst);
	vThresholdIsoData(pDst,pDst,BRIGHT);
	pDst->lut = LUT_BINARY;
	pc_send_image(pDst);
  pc_send_string("2. vThresholdIsoData");
	
	
	benchmark_start(&bench, "vRemoveBorderBlobs");
  vRemoveBorderBlobs(pDst,pDst,EIGHT);
  benchmark_stop(&bench);
  pc_send_benchmark(&bench);
  pDst->lut = LUT_BINARY;
  pc_send_image(pDst);
  pc_send_string("3. vRemoveBorderBlobs");
	
	
	
  // 2. Start a benchmark, execute vision operation and send info to PC
  benchmark_start(&bench, "vFillHoles");
  vFillHoles(pDst, pDst, EIGHT);
  benchmark_stop(&bench);
  pc_send_benchmark(&bench);
  pDst->lut = LUT_BINARY;
  pc_send_image(pDst);
  pc_send_string("4. vFillHoles");
  
	
  // Notice that from now on the destination image also is the source image!!
  
  // 3. Start a benchmark, execute vision operation and send info to PC
  benchmark_start(&bench, "vFindEdges");
  vFindEdges(pDst, pDst, EIGHT);
  benchmark_stop(&bench);
  pc_send_benchmark(&bench);
  pDst->lut = LUT_BINARY;
  pc_send_image(pDst);
  pc_send_string("5. vFindEdges");
}
