#include "VisionSet.h"

void VisionSet8(image_t *pSrc, image_t *pDst)
{
  benchmark_t bench;
 
  // 1. Original source image to PC
  pc_send_string("1. Source image");
  pSrc->lut = LUT_CLIP;
  pc_send_image(pSrc);

  // 2. Start a benchmark, execute vision operation and send info to PC
  benchmark_start(&bench, "ContrastStrtchF");
  vContrastStretchFast(pSrc, pDst);
  benchmark_stop(&bench);
  pc_send_benchmark(&bench);
  pDst->lut = LUT_STRETCH;
  pc_send_image(pDst);
  pc_send_string("2. ContrastStretchFast");
  
  // Notice that from now on the destination image also is the source image!!
  
  // 3. Start a benchmark, execute vision operation and send info to PC
  benchmark_start(&bench, "Threshold");
  vThreshold(pDst, pDst, 0, 100);
  benchmark_stop(&bench);
  pc_send_benchmark(&bench);
  pDst->lut = LUT_BINARY;
  pc_send_image(pDst);
  pc_send_string("3. Threshold 0-100");
}
