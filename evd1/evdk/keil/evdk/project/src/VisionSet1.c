#include "VisionSet.h"
#include "led.h"

// This vision set is executed when the system powers on

/* Shape Detect */

void VisionSet1(image_t *pSrc, image_t *pDst)
{
  benchmark_t bench;
	static uint8_t labels;
	static blobinfo_t blobs[16];
	int i;

  // 1. Original source image to PC
  pc_send_string("1. Source image");
  pSrc->lut = LUT_CLIP;
  pc_send_image(pSrc);

  // 2. PreProcessing
  benchmark_start(&bench, "vContrastStretchFast");
		vContrastStretchFast(pSrc,pDst);
	benchmark_stop(&bench);
	pc_send_benchmark(&bench);
	benchmark_start(&bench, "vThresholdIsoData");
		vThresholdIsoData(pDst,pDst,DARK);
	benchmark_stop(&bench);
	pc_send_benchmark(&bench);
	benchmark_start(&bench, "vRemoveBorderBlobs");
		vRemoveBorderBlobs(pDst,pDst,EIGHT);
	benchmark_stop(&bench);
	pc_send_benchmark(&bench);
	benchmark_start(&bench, "vFillHoles");
		vFillHoles(pDst,pDst,EIGHT);
	benchmark_stop(&bench);
  pc_send_benchmark(&bench);
  pDst->lut = LUT_BINARY;
  pc_send_image(pDst);
  pc_send_string("2. vPreProces");

	// 3. Start a benchmark, execute vision operation and send info to PC
  benchmark_start(&bench, "iLabelBlobs");
		labels = iLabelBlobs(pDst,pDst,FOUR);
  benchmark_stop(&bench);
  pc_send_benchmark(&bench);
  pDst->lut = LUT_LABELED;
  pc_send_image(pDst);
  //pc_send_string("4. iLabelBlobs");
	
	if(labels < 16 && labels != 0){
		benchmark_start(&bench, "vBlobAnalyse");
			vBlobAnalyse(pDst,labels,blobs);
		benchmark_stop(&bench);
		pc_send_benchmark(&bench);
		pDst->lut = LUT_LABELED;
		pc_send_image(pDst);
		pc_send_string("5. vBlobAnalyse");
		
		for(i=0; i<labels; i++){
			blobs[i].perimeter = blobs[i].perimeter*blobs[i].perimeter / (double)blobs[i].nof_pixels;
			if(blobs[i].perimeter > 19.0){
				pc_send_string("Triangle");
				led_set_bus((1<<LED1)|(1<<LED2)|(1<<LED3));
			}else
			if(blobs[i].perimeter > 15.0){
				pc_send_string("Square");
				led_set_bus(15);
			}else
			if(blobs[i].perimeter > 13.0){
				pc_send_string("Circle");
				led_set_bus((1<<LED1));
			}else{
				pc_send_string("Blob onbekend");
				led_set_bus(0);				
			}
		}
	}else{
			pc_send_string("2. iLabelBlobs\nErr: label overflow");
	}
}
