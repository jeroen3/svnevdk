VisionSVN Jeroen Lodder (480060) EVD Minor 2013-2014

Visionsets Qt:
1	image to bcd
2	3 digit display test
3	Add/Threshold/Erase/SetBorders
4	image to bcd (oud)
5	Fillholes / findedges
6	Qtimer / Copy / ContrastStretch / Threshold / Erase
7	mem test
8	ContrastStretchFast / Threshold
9	Shape Detect

Visionsets EVDK:
1	Shape Detect
2	(Dark) Digit detect
3	vAdd/vThresholdIsoData/vMultiply/vSetBorders/vBitSlicing
4	vContrastStretch/vContrastStretchFast/vRotate180/vThreshold/vThresholdIsoData
5	vThresholdIsoData / vRemoveBorderBlobs  /vFillHoles/vFindEdges
6	mem test
7	(BRIGHT) Digit detect
8	default
9	default

Non-default file level build settings:
user group : -03
user group : -03
operators.cpp: do not build
extra-operators.cpp: do not build

Target compiler optimiser: -O1 -Otime

